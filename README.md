# RPA and GW for Molecules and Solids

Find the current tutorial website here:

[https://fhi-aims-club.gitlab.io/tutorials/rpa-and-gw-for-molecules-and-solids](https://fhi-aims-club.gitlab.io/tutorials/rpa-and-gw-for-molecules-and-solids)

## How to locally setup the website

1. Clone the project:
    ```
    git clone git@gitlab.com:FHI-aims-club/tutorials/rpa-and-gw-for-molecules-and-solids.git
    ```
2. Go into the folder `rpa-and-gw-for-molecules-and-solids` and install the requirements:
    ```
    pip install -r requirements.txt
    ```
3. Run:
   ```
    mkdocs serve --strict
   ```

Mkdocs now builds the website and makes it available under `localhost:8000` (you can just enter this in your browser). The port might be a different on your platform, but mkdocs tells you where to find the website. 

Mkdocs now watches your changes an rebuilds when ever you change something in the docs (you need to save your changes). So you can watch your changes. 

## Publish your changes

After finalizing your changes, please don't forget to add, commit and push to the remote repo:

1. Add all (if applicable):
    ```
    git add -A
    ```
2. Commit and describe your changes:
    ```
    git commit -m "My contributions"
    ```
3. Pull first:
    ```
    git pull
    ```
4. Push now:
    ```
    git push
    ```

The gitlab CI/CD pipeline will now build the website and deploy it to the following page:

[https://fhi-aims-club.gitlab.io/tutorials/rpa-and-gw-for-molecules-and-solids](https://fhi-aims-club.gitlab.io/tutorials/rpa-and-gw-for-molecules-and-solids)
