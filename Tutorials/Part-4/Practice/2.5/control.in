#  Volker Blum, FHI 2004 : Input file for LocalOrb code project
#
#  Input file control.in : All computational details for structure geometry.in
#
#  * First, general computational parameters:
#
#  Physics choices (model):
#
#  xc               hf
  xc                pbe
#  spin             collinear
#  default_initial_moment  0
#  charge 1 
  relativistic     none
  occupation_type  gaussian 0.00001
#  total_energy_method rpt2
  total_energy_method rpa
#   rpa_correlation_energy  ex_combo
#   rpa_correlation_energy  without_ex_combo
#    rpa_correlation_energy  restricted_closed_shell_singlet
#   rpa_correlation_energy    ph_through_diagonalization
   frequency_points    16
#   frozen_core_postSCF 1
   use_2d_corr .false.
   
#  state_lower_limit 3
#
#  SCF cycle:
#
  KS_method        parallel
  mixer            pulay 
    n_max_pulay    5
    charge_mix_param  0.6
  override_illconditioning .true.
#  preconditioner kerker 2.0
  sc_accuracy_rho  1E-8
  sc_accuracy_eev  1E-6
  sc_accuracy_etot 1E-8
  sc_iter_limit    100 
#
#  accuracy / efficiency for normal scf
#
  basis_threshold  1.e-5
#  distributed_spline_storage .true.
#
#  For MP2
#
#  hf_version 0 
  empty_states     1000
#  prodbas_threshold      1.e-4
#  auxil_basis     full
#  partition_acc 1e-20
#  use_density_matrix_hf   .true.
#  prodbas_threshold 1.E-3
#
#  For periodic
#
#  k_grid 12 12 1 
#  use_density_matrix
#  packed_matrix_format index
#  output k_eigenvalue 1
#  restart  cu.slab.CO.top.pbe.relax
#  restart_save_iterations 3
#
# For relaxation:
#
#  relax_geometry bfgs 1e-2
#  sc_accuracy_forces 1e-4
#  * Next, specify output options:
#    - basis
#    - matrices
#    - grids
#    - v_eff
#    - v_hartree (partitioned Hartree potentials)
#
#  output       basis
#  output       matrices
#  output       grids
#  output       v_eff
#  output       v_hartree
#
################################################################################
#
#  FHI-aims code project
# Igor Ying Zhang and Xinguo Ren, Fritz Haber Institute Berlin, 2012
#
#  Suggested "cc-pVTZ" defaults for H atom (to be pasted into control.in file)
#
################################################################################
  species        H
#     global species definitions
    nucleus             1
    mass                1.00794
#
    l_hartree           6
#
    cut_pot             4.0  2.0  1.0
    basis_dep_cutoff    0e-0
#
   max_n_prodbas          10
   max_l_prodbas          10     
   prodbas_acc   1.E-6
    radial_base         24 7.0
    radial_multiplier   6
    angular_grids       specified
      division   0.1930   50
      division   0.3175  110
      division   0.4293  194
      division   0.5066  302
      division   0.5626  434
      division   0.5922  590
      division   0.6227  974
#      division   0.6868 1202
      outer_grid   974
#      outer_grid  770
#      outer_grid  434
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      1  s   1.
#     ion occupancy
    ion_occ      1  s   0.5
################################################################################
#
#   For exact comparison with all GTO-based codes, one or more of
#   the following flags are needed:
#
    include_min_basis   .false.
    pure_gauss          .true.
#


# H cc-pVTZ
 gaussian 0 3
        33.8700000            0.0060680  
         5.0950000            0.0453080  
         1.1590000            0.2028220  
 gaussian 0 1 0.3258000
 gaussian 0 1 0.1027000
 gaussian 1 1 1.4070000
 gaussian 1 1 0.3880000
 gaussian 2 1 1.0570000
