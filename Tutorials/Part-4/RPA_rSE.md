# RPA and beyond for molecules
In this tutorial, we will learn how the random phase approximation (RPA) total energy plot of hydrogen molecule is obtained. We perform the RPA total energy calculation using the PBE potenetial. Once the scf cycle converged, the exact exchange and RPA correlation energy is calculated in the post-scf manner.

In FHI-aims, a RPA calculation can be requested using the following keywords in the `control.in` file:

```
  xc   pbe
  relativistic     none
  occupation_type  gaussian 0.005
  total_energy_method  rpa
  frequency_points  16
```
The `geometry.in` file is set as 

```
  atom         0.0000         0.0000      0.0000 H
  atom         0.0000         0.0000      0.3000 H
```
In the `geometry.in` file, the distance is 0.3 Å between two hydrogen atoms. We change the distance by 0.1 Å and perform the calculation upto 5.0 Å distance between two atoms. Once the calculations completed, then at the end of output file we find the following output pattern
```
 -----------------------------------------------------------------------------
    RPA correlation energy :          -0.07157638  Ha,        -1.94769239  eV
 
 --------------------------------------------------------------------
    Exact exchange energy             :           -0.85883430 Ha,         -23.37007049 eV
    DFT/HF exchange energy            :           -0.73728354 Ha,         -20.06250579 eV
 
    DFT/HF total energy               :           -0.68236586 Ha,         -18.56811975 eV
    Exchange-only total energy        :           -0.65505678 Ha,         -17.82500188 eV
    RPA total energy                  :           -0.72663316 Ha,         -19.77269427 eV
 
    RPA+SE total energy               :           -0.72762458 Ha,         -19.79967209 eV
    RPA+rSE (diag) total energy       :           -0.72728166 Ha,         -19.79034089 eV
    RPA+rSE total energy              :           -0.72729866 Ha,         -19.79080349 eV
 
    RPA+ total energy                 :           -0.69080574 Ha,         -18.79778070 eV
 
 ----------------------------------------------------------------------------
```
The `DFT/HF total energy` is the PBE total energy, `Exchange-only total energy` is the Hartree-Fock energy on the top of PBE potential. If we use the HF potential for calculation then `DFT/HF total energy` and `Exchange-only total energy` are same. The `RPA total energy` is  $E^{HF} + E^{RPA}_{c}$ correlation energy. Similarly, `RPA+SE total energy` is the $E^{HF} + E^{RPA}_{c}+E^{SE}_{c}$ energy and `RPA+rSE total energy` is the $E^{HF} + E^{RPA}_{c}+E^{rSE}_{c}$ energy. Below, we collect the data for RPA total energy which is later used to draw the plot.  
```
D(Å)      E(Ha)        D(Å)      E(Ha)       D(Å)     E(Ha)        D(Å)     E(Ha) 
0.3   -0.72663316      1.4   -1.11444751     2.5   -1.01670750    3.6   -1.00563707
0.4   -1.01106276      1.5   -1.09948454     2.6   -1.01362027    3.7   -1.00664190        
0.5   -1.13552106      1.6   -1.08580675     2.7   -1.01114656    3.8   -1.00834082        
0.6   -1.18847916      1.7   -1.07346633     2.8   -1.00921571    3.9   -1.01079615        
0.7   -1.20624139      1.8   -1.06245431     2.9   -1.00775992    4.0   -1.01390341        
0.8   -1.20585043      1.9   -1.05272288     3.0   -1.00670764    4.1   -1.01733853        
0.9   -1.19601556      2.0   -1.04420092     3.1   -1.00597974    4.2   -1.02054217        
1.0   -1.18143696      2.1   -1.03680492     3.2   -1.00549607    4.3   -1.02275615        
1.1   -1.16475473      2.2   -1.03044638     3.3   -1.00519597    4.4   -1.02312034        
1.2   -1.14749260      2.3   -1.02503594     3.4   -1.00506745           
1.3   -1.13054841      2.4   -1.02048513     3.5   -1.00517032            

```
<a name="H2-RPA-total-energy"></a>
![RPA](../img/RPA.png)

