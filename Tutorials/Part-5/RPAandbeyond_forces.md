# RPA and beyond forces
In this tutorial, we will learn how the structure relaxation is performed using the RPA gradients. For this test, water molecule is selected for structure relaxation. Currently, the RPA based relaxation can be performed using local density approximation of Perdew and Zunger (keyword in FHI-aims: `xc   pz-lda`) or generalized gradient approximation of Perdew, Burke, and Ernzerh of (keyword in FHI-aims `xc  pbe`) potentials. But, here we will perform the RPA based structure relaxation using PBE potential.  

The structure relaxation based on RPA@PBE gradients can be requested using the following keywords in `control.in` file

```
  xc   pbe
  relativistic     none
  occupation_type  gaussian 0.001
  RI_method lvl                   
  DFPT  vibration_reduce_memory
  relax_geometry bfgs 1e-2
  sc_accuracy_forces 1e-2
  least_memory_4 .true.
  rpa_force  freq_formula_method 
  frequency_points  12
  my_prodbas_threshold 1.E-12
```
In case to request RPA+rSE relaxation, an additional keyword is required

  ```
  post_SCF_force rSE
 ```

In the output file, we have following pattern 

```
  -----------------------
  atom #    1
   Hellmann-Feynman              : -0.153483E+02  0.222536E-12  0.867247E+01
   Ionic forces                  :  0.000000E+00  0.000000E+00  0.000000E+00
   Multipole                     :  0.750483E-01 -0.115005E-12 -0.430620E-01
   Hartree-Fock exchange         :  0.000000E+00  0.000000E+00  0.000000E+00
   RPA forces                    :  0.132245E+02  0.163446E-04 -0.746856E+01
   Pulay                         :  0.196802E+01 -0.176250E-04 -0.110770E+01
   ----------------------------------------------------------------
   Total forces(   1)            : -0.807040E-01 -0.128042E-05  0.531424E-01
  atom #    2
   Hellmann-Feynman              :  0.559863E+00  0.496474E-14  0.246783E+00
   Ionic forces                  :  0.000000E+00  0.000000E+00  0.000000E+00
   Multipole                     :  0.247599E-01  0.434865E-15 -0.184421E-01
   Hartree-Fock exchange         :  0.000000E+00  0.000000E+00  0.000000E+00
   RPA forces                    : -0.116235E+02 -0.444885E-04 -0.514752E+01
   Pulay                         :  0.111061E+02  0.473977E-04  0.494383E+01
   ----------------------------------------------------------------
   Total forces(   2)            :  0.671756E-01  0.290925E-05  0.246513E-01
  atom #    3
   Hellmann-Feynman              :  0.793644E-01  0.133055E-13 -0.615867E+00
   Ionic forces                  :  0.000000E+00  0.000000E+00  0.000000E+00
   Multipole                     :  0.285948E-01  0.267958E-15 -0.116149E-01
   Hartree-Fock exchange         :  0.000000E+00  0.000000E+00  0.000000E+00
   RPA forces                    : -0.160101E+01 -0.229104E-04  0.126155E+02
   Pulay                         :  0.150797E+01  0.241409E-04 -0.120670E+02
   ----------------------------------------------------------------
   Total forces(   3)            :  0.149186E-01  0.123053E-05 -0.789564E-01
```

The force due to exact-exchange energy gradient is also added to RPA correlation part of force. As exact-exchange and RPA correlation gradients are $Tr[\dfrac{d(E^{ex}_{x}+E^{RPA}_{c})}{dc}\dfrac{dc}{dR_{A}}]$, $Tr[\dfrac{d(E^{ex}_{x}+E^{RPA}_{c})}{dV}\dfrac{dV}{dR_{A}}]$, $Tr[\dfrac{d(E^{ex}_{x}+E^{RPA}_{c})}{dC}\dfrac{dC}{dR_{A}}]$ and so on. Here $c$ are Kohn-Sham eigenvector, $V$ is the Coulomb matrix in auxiliary basis and $C$ are resolution of identity coefficients. For further details, we refer the readers to reference Muhammad et al., J. Phys. Chem. A 2024, 128, 37, 7939–7949. 

We start the structure relaxation  for H$_{2}$O molecule with the following `geometry.in` file

``` 
     atom        -0.07712649        0.00000000        1.49704522  O
     atom         0.82002231        0.00000000        1.86358518  H
     atom         0.07269418       -0.00000000        0.53972961  H
```

We performed the geometry optimization using Numerical Atomic Orbital (NAO) light (first-tier) basis sets and found the following relaxed structure for H$_{2}$O molecule

```  
  Final atomic structure:
                         x [A]             y [A]             z [A]
            atom        -0.07784424        0.00000000        1.49751817  O
            atom         0.82081992        0.00000000        1.86417348  H
            atom         0.07261432        0.00000000        0.53866837  H
```
