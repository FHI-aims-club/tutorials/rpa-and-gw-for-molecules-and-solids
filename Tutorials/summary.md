# Summary

This final chapter briefly summarizes which keywords are needed to be set for RPA, GW, BSE calculations. Additionally, some information are added about the available non-standard basis functions, which are helpful for reaching convergence for the beyond-DFT methods.

## GW calculations

The GW calculation requires a set of KS eigenstates. Thus, normally a DFT calculation is performed prior to starting the actual GW calculation. The choice of the XC functional for the DFT calculation may affect the results of the G0W0 calculation. A G0W0@PBE calculation is triggered by the following keywords in the `control.in` file:
```
xc                pbe
relativistic      atomic_zora scalar
k_grid            <k_x> <k_y> <k_z> #if periodic
qpe_calc          gw
anacon_type       <0 or 1>
frequency_points  <N>

[...]
```

In addition, for a periodic G0W0 calculation, one must request band structure output in the `control.in` file:
```
output band k_xstart k_ystart k_zstart k_xend k_yend k_zend n_points symbol_start symbol_end
```

Check the following parameters for convergence:

* Number of frequency points
* Number of basis functions
* (if periodic) Number of k-points.


## RPA calculation

The RPA calculation is short for the calculation of the RPA correlation energy and the exact exchange energy. It can be requested in the `control.in` by adding (you will also need to include the common DFT setting, such as `xc` etc.):
```
xc                     pbe
relativistic           atomic_zora scalar
k_grid                 <k_x> <k_y> <k_z> #if periodic
total_energy_method    rpa
frozen_core_postscf    <N>

[...]
```

The second line activates the frozen core approximation and is an optional keyword. It tells FHI-aims to use only the `N` highest occupied shells and freeze the remaining shells from the post-scf calculation (in our case RPA).

Check the following parameters for convergence:

* Number of basis functions
* (if periodic) Number of k-points.

## BSE calculation (molecules only)

To request the BSE calculation, the following settings in the `control.in` are needed to be set (here, PBE is used for the DFT part of the calculation):
```
# DFT settings

xc                       pbe
relativistic             atomic_zora scalar
override_illconditioning .true.
basis_threshold          1.0e-6

# Make sure all the unoccupied orbitals are calculated
calculate_all_eigenstates 

######################################################

# GW settings

qpe_calc                 gw   # G0W0
anacon_type              1    # PADE
n_anacon_par             16

#####################################################

# BSE settings

neutral_excitation       bse
read_write_qpe           rw

# singlet or triplet excitation
bse_s_t                  singlet 

# reduced BSE matrix for computational efficiency               
#casida_reduce_matrix .true.   
#casida_reduce_occ -10000.0
#casida_reduce_unocc 20.0
```

## Available non-standard basis sets for beyond-DFT methods 

### Valence correlation-consistent basis function

For the elements 1-18 valence correlation-consistent basis function[^1] can be found in the directory:
```
AIMS_DIR/species_defaults/NAO-VCC-nZ
```
where `n` refers to double zeta, triple zeta, etc. These basis sets are suited for the infinite-basis-set extrapolation. 

### `Tier2_aug2` basis functions for BSE

Especially tested for *BSE calculations*[^2], you can find for elements 1-17 the "tier 2 + 2 Gaussian" species defaults here:
```
AIMS_DIR/species_defaults/non-standard/Tier2_aug2
```

### Dunning basis set

These basis sets are suited for the infinite-basis-set extrapolation. 
```
AIMS_DIR/species_defaults/non-standard/gaussian_tight_770/cc-pVnZ/
```
where `n` refers to double zeta, triple zeta, etc. These basis sets are suited for the infinite-basis-set extrapolation. 

### `_gw` species defaults for periodic GW

The basis sets *intermediate_gw*, *tight_gw* and *really_tight_gw*, which are located in the same `species_defaults/defaults_2020` directory, have been tested and can lead to improvements in accuracy for periodic GW. Although they are similar to the regular basis sets, they incorporate `for_aux` basis functions that are used to build the auxiliary basis set for representing the Coulomb operator. We recommend their use for GW calculations with solids, although note that additional computational cost is incurred by using them.


### Basis-set-convergence strategies for heavier elements

Adding more and more additional basis functions from the third tier will not necessarily improve the convergence as other issues will pop up. Going beyond tier 2 results in linear dependent basis functions and will already cause numerical issues during the DFT part of the calculation. 

To avoid this, Ren et al.[^3] demonstrated an alternative approach for G0W0 calculations using highly localized Slater-type orbitals (STOs). This approach of adding additional STOs on top of tier 2 is transferrable to the RPA and BSE approaches as well.

[^1]:
  Zhang, Igor Ying, et al. "Numeric atom-centered-orbital basis sets with valence-correlation consistency from H to Ar." New Journal of Physics 15.12 (2013): 123033.

[^2]:
  [Liu, Chi, et al. "All-electron ab initio Bethe-Salpeter equation approach to neutral excitations in molecules with numeric atom-centered orbitals." The Journal of chemical physics 152.4 (2020): 044105.](https://aip.scitation.org/doi/pdf/10.1063/1.5123290)

[^3]:
  Ren, Xinguo, et al. "All-electron periodic G0W0 implementation with numerical atomic orbital basis functions: Algorithm and benchmarks." Physical Review Materials 5.1 (2021): 013807. Cf. section C.2.