As an introduction to this topic we also recorded two lectures. Please find them below. The corresponding lecture slides can be found here:

* [Xingou Ren: Random-phase approximation](https://indico.fhi-berlin.mpg.de/event/112/contributions/623/attachments/241/737/Xinguo%20Ren%20FHI-aims%20Talk%20September%202021.pdf)
* [Dorothea Golze: Excited State Formalisms for Charge Carriers / GW and GW+BSE](https://indico.fhi-berlin.mpg.de/event/112/contributions/624/attachments/239/735/Golze%20Dorothea%20FHI-aims%20Talk%20September%202021.pdf)

## Xingou Ren: Random-phase approximation

<iframe width="560" height="315" src="https://www.youtube.com/embed/wdeqULriVFY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Dorothea Golze: Excited State Formalisms for Charge Carriers / GW and GW+BSE

<iframe width="560" height="315" src="https://www.youtube.com/embed/Kf6cY0PA8Sw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

