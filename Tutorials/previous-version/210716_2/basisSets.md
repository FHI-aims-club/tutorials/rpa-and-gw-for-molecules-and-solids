# Basis sets for correlated methods

Explicitly correlated methods (as opposed to ground state DFT) require more attention to basis set convergence than ground state DFT calculations themselves. This section provides a brief introduction to our current available basis set approaches to explicitly correlated methods. More detailed accounts can be found in the references provided at the end of this section.

**Background**

The random-phase approximation (RPA) for total energies, GW for electronic single-particle like excitations, and the Bethe-Salpeter Equation (BSE), which models the neutral (e.g., optical) excitations of a system, are all examples of "explicitly correlated" methods. In these and similar high-level methods, the electronic correlation terms enter directly, usually integrated all over space, but in the form of terms that estimate "if one electron is located at $r$, what is the probability to find another electron at a different location $r'$?" 

In contrast, ground-state DFT relies on the electron density $n(r)$ or the occupied single-particle orbitals of a system. Those objects related to terms of the form "what is the probability for finding an electron at a location $r$?", but ground state density functional approximations do not usually require one to evaluate terms of the form $p(r,r')$, where $p$ is an estimated probability related to finding two electrons in locations $r$ and $r'$ together.

The mathematical difference between dealing with objects of the form $n(r)$ vs. $p(r,r')$ has consequences for the degree of convergence that can be achieved with a given basis set. 

In general, objects such as $n(r)$ are fairly smooth in spatial regions away from the nucleus and can therefore be well represented with basis sets that resolve the near-nuclear density well and provide some additional flexibility to describe deviations of an overall smooth density from the atomic limit in other regions of space.

In contrast, objects such as $p(r,r')$ can require high resolution, equivalent to needing larger basis sets, in regions of space in which ground-state DFT is already represented well by a few basis functions, but in which sharp peaks ("cusps") of $p(r,r')$, for values $|r-r'|\rightarrow 0$, require higher resolution to be represented with high precision.

The species defaults for ground state DFT in FHI-aims, i.e., "light", "intermediate", "tight", and the associated basis set "tiers" (or levels), can provide excellent numerical convergence for total energies and other quantities derived from DFT. However, observables derived explicitly correlated methods will, in general, not converge nearly as well with the standard species defaults intended for DFT and more care is required.

**Basis sets for all-electron, correlated methods**

The following list is a partial account of basis sets available for RPA, GW, or BSE calculations for use with FHI-aims and their respective area of applicability.

- **FHI-aims species defaults for ground-state DFT**: "light-spd", "light", "intermediate", "tight", and associated basis sets:

    These basis sets, summarized in Ref. [1] and called FHI-aims-2009 for short, provide uniformly high accuracy for ground state DFT. They are available for essentially all relevant chemical elements (1-102) and they provide high numerical precision for the DFT parts of the total energy especially near the nucleus. This is their strength. 
    
    The FHI-aims-2009 can, in fact, also be used for correlated calculations. For heavier elements, they may be the only available all-electron option, but caution is in order. 
    
    Total energies, which are usually very well converged in DFT, will not be well converged in RPA. Energy differences, such as binding energies between atoms and molecules, can still be calculated, but a so-called counterpoise correction is essential to reduce the effect of basis set superposition errors (see, e.g., Ref. [2] for examples, as well as this tutorial).
    
    In GW, results can be qualitatively reasonable especially for the larger FHI-aims-2009 basis sets but convergence should be checked by other means if possible. Ref. [2] shows examples for molecules (as do many other references) and Ref. [3] shows examples for some solids.

- **Correlation consistent Gaussian-type basis sets** by Dunning and others, as well as basis set extrapolation:

    For light elements, Gaussian-type correlation consistent basis sets - see Ref. [4] and several subsequent references - provide a de facto standard in quantum chemistry for correlated methods. In FHI-aims, high-precision pre-tabulated versions of many Gaussian basis sets are provided in the directory _species_defaults/non-standard/gaussian_tight_770_. Together with a suitable basis set extrapolation strategy to the complete basis set limit, they can and have been used for high-precision correlated reference results using FHI-aims by many authors. The format of the Gaussian-type species defaults in FHI-aims is straightforward and any other Gaussian-type basis set can be adapted for use in FHI-aims as well.

    One limitation is that correlation consistent basis sets all-electron basis sets typically cannot be found for elements beyond the 3d transition metals. In our limited experience, elements 1-18 are safely covered, whereas the correlation consistent basis sets for K (Z=19) and above can need some detailed attention when attempting to obtain basis set converged results. When applying correlated methods for heavier elements, the all-electron FHI-aims-2009 basis sets provide a qualitative way forward but, as mentioned above, also need to be checked for convergence carefully.

    Another caveat is that the Gaussian-type species defaults provided with FHI-aims include integration grids that are very dense and optimized for high precision (reference purposes), not speed. One can make these defaults significantly more efficient by reducing the grid requirements. However, highly contracted Gaussian-type orbital basis functions do require denser integration grids than numeric atom-centered basis functions, as shown in Figure A.2 in Ref. [5].

    Augmented Gaussian-type correlation consistent basis sets (e.g., Ref. [6]) are also available. These basis sets add very extended Gaussian type orbitals and can be useful for quantities that rely on electron affinities in molecules (i.e., neutral excitations systems with a boundary and with extended orbitals). For solids, augmentation functions are generally problematic since the augmentation functions introduce a very high number of non-zero basis functions at any given point (extended basis functions from different unit cells overlap) and lead to mathematically unstable ("ill-conditioned") basis sets.

    Many other general and special-purpose correlation consistent basis sets can be found in the literature, e.g., including better support for core states or extensions of the original correlation consistent recipe. We refer to the basis set exchange, https://www.basissetexchange.org/, for such basis sets.

- **NAO-VCC-nZ correlation consistent basis sets**:

    For elements 1-18 (H-Ar), a group of correlation consistent numeric atom-centered basis sets (NAO-VCC-nZ) developed for total energies (RPA and MP2) is available, including a basis set extrapolation strategy. These basis sets are numerically the most efficient ones when seeking high precision for RPA total energies of light-element molecules. A full description is given in Ref. [5].

- **"tier2+aug2" basis sets** (Bethe-Salpeter Equation for low-lying neutral excitations in molecules):

    For low-energy neutral excitations (e.g., optical excitations) including excitonic effects in molecules, the BSE/GW formalism can be used. For such systems, a simple basis set recipe provides numerically highly precise results close to basis set convergence: The "tier2" version of the FHI-aims-2009 basis sets, combined with the two lowest angular momentum augmentation functions from Dunning's augmented basis sets. These species defaults are called "tier2+aug2" for short and, for some light elements, can be found in the directory _species_defaults/non-standard/Tier2_aug2_. Extension of any other FHI-aims-2009 species defaults file is trivially possible by editing the species defaults files in the same way as done for the existing "tier2+aug2" examples.

    In-depth benchmarks of the "tier2+aug2" basis sets for BSE/GW are given in Ref. [7].

**References**

[1] Volker Blum, Ralf Gehrke, Felix Hanke, Paula Havu, Ville Havu, Xinguo Ren, Karsten Reuter, and Matthias Scheffler,
_Ab initio molecular simulations with numeric atom-centered orbitals_, Computer Physics Communications 180, 2175-2196 (2009). http://dx.doi.org/10.1016/j.cpc.2009.06.022.

[2] Xinguo Ren, Patrick Rinke, Volker Blum, Jürgen Wieferink, Alex Tkatchenko, Andrea Sanfilippo, Karsten Reuter, and Matthias Scheffler,
_Resolution-of-identity approach to Hartree-Fock, hybrid density functionals, RPA, MP2, and GW with numeric atom-centered orbital basis functions_,
New Journal of Physics 14, 053020 (2012). http://stacks.iop.org/1367-2630/14/053020.

[3] Xinguo Ren, Florian Merz, Hong Jiang, Yi Yao, Markus Rampp, Hermann Lederer, Volker Blum and Matthias Scheffler, _All-electron periodic G0W0 implementation with numerical atomic orbital basis functions: algorithm and benchmarks_, Physical Review Materials 5, 013807 (2021). http://dx.doi.org/10.1103/PhysRevMaterials.5.013807.

[4] Thom H. Dunning Jr., _Gaussian basis sets for use in correlated molecular calculations. I. The atoms boron through neon and hydrogen_, J. Chem. Phys. 90, 1007 (1989); https://doi.org/10.1063/1.456153.

[5] Igor Ying Zhang, Xinguo Ren, Patrick Rinke, Volker Blum, and Matthias Scheffler, _Numeric atom-centered-orbital basis sets with valence-correlation consistency from H to Ar_, New Journal of Physics 15, 123033 (2013). http://iopscience.iop.org/1367-2630/15/12/123033/article.

[6] R. A. Kendall, T. H. Dunning, and R. J. Harrison, _Electron affinities of the first-row atoms revisited. Systematic basis sets and wave functions_, J. Chem. Phys. 96, 6796–6806 (1992). https://doi.org/10.1063/1.456153.

[7] Chi Liu, Jan Kloppenburg, Yi Yao, Xinguo Ren, Heiko Appel, Yosuke Kanai, Volker Blum
All-electron ab initio Bethe-Salpeter equation approach to neutral excitations in molecules with numeric atom-centered orbitals
The Journal of Chemical Physics, 152, 044105 (2020). https://aip.scitation.org/doi/full/10.1063/1.5123290.



