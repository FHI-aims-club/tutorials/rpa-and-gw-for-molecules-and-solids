#!/bin/bash -l

# Standard output and error:
#SBATCH -o ./tjob.out.%j
#SBATCH -e ./tjob.err.%j
# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J nothread
# Queue (Partition):
#SBATCH --partition=medium
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=10
#SBATCH --ntasks-per-node=40
#
#SBATCH --mail-type=none
#SBATCH --mail-user=mnakh@rzg.mpg.de
#
# Wall clock limit:
#SBATCH --time=1-00:00:00

ulimit -s unlimited
export OMP_NUM_THREADS=1

module purge
module load intel/19.1.2 impi/2019.8 mkl/2020.2 arm_ddt/21.0






currentfolder=$(pwd)
logfile="$currentfolder/job.log"

echo "parent: $currentfolder" >> $logfile
echo "logfile: $logfile" >> logfile

echo "" >> $logfile
echo "" >> $logfile
echo "" >> $logfile
echo "..............................................." >> $logfile
echo "................Start a job................" >> $logfile
echo "..............................................." >> $logfile
date "+%d/%m/%Y  %H:%M:%S" >> $logfile
echo "" >> $logfile

list=$(find -name 'control.in' -printf '%h\n' | sort --unique)

FILE="geometry.in"
for item in $list
do
   cd $item
   if [ -f $FILE ]; then
      echo "Enter to dir: $item" >> $logfile
      date "+%d/%m/%Y  %H:%M:%S" >> $logfile
      #srun aims.180218.scalapack.mpi.x > output
      #ddt --connect srun aims.210716.scalapack.thread.debug.mpi.x
      srun aims.210716_2.scalapack.mpi.x > aims.out
   fi
   cd $currentfolder
   echo "enter to dir: $currentfolder" >> $logfile
   date "+%d/%m/%Y  %H:%M:%S" >> $logfile
   echo "" >> $logfile
done



