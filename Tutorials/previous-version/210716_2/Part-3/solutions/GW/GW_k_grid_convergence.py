from pathlib import Path
from matplotlib import pyplot as plt
import numpy as np

p = Path(".")

k_grids = ["222", "444", "888"]
k_labels = ["2x2x2", "4x4x4", "8x8x8"]
species_defaults = ["tight-tier1"]
frequencies = [60]


gaps = {}

for s in species_defaults:
    gfreq = []
    for k in k_grids:
        out = p / f"T1-{k}-f60" / "aims.out"
        with open(out) as o:
            lines = o.readlines()

        for line in lines:
            if line.startswith("  | Energy difference      :"):
                t = float(line.split()[4])
        gfreq.append(t)

    print(gfreq)
    plt.plot(np.arange(len(k_grids)), gfreq)

plt.title("GW k-grid convergence")
plt.xticks(np.arange(3), k_labels)
plt.xlabel("k-grid")
plt.ylabel("Indirect gap (eV)")
plt.show()
