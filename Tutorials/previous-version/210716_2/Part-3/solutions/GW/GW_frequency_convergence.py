from pathlib import Path
from matplotlib import pyplot as plt
import numpy as np

p = Path(".")

k_grids = ["k_grid_2x2x3"]
species_defaults = ["tight-tier1"]
frequencies = [20, 40, 60, 80, 100]


gaps = {}

for s in species_defaults:
    for k in k_grids:
        gfreq = []
        for f in frequencies:
            out = p / f"T1-444-f{str(f)}" / "aims.out"
            with open(out) as o:
                lines = o.readlines()

            for line in lines:
                if line.startswith("  | Energy difference      :"):
                    t = float(line.split()[4])
            gfreq.append(t)

        print(gfreq)
        plt.plot(frequencies, gfreq)

plt.title("GW frequency convergence")
plt.xlabel("Number of frequency points")
plt.ylabel("Indirect gap (eV)")
plt.show()
