from pathlib import Path
from matplotlib import pyplot as plt
import numpy as np

p = Path(".")

k_grids = ["k_grid_2x2x3"]
k_labels = ["2x2x3"]
species_defaults = ["tight-tier1", "tight-tier2"]
frequencies = [20]


gfreq = []
for s in species_defaults:

    out = p / s / "k_grid_2x2x3" / f"frequency_20" / "aims.out"
    with open(out) as o:
        lines = o.readlines()

    for line in lines:
        if line.startswith("  | Energy difference      :"):
            t = float(line.split()[4])
    gfreq.append(t)

print(gfreq)
plt.plot(np.arange(2), gfreq)

plt.show()
