# GW for rutile TiO2

In the previous chapter we were able to correctly predict the structure of TiO2 using the RPA correlation energy. However, the total energy is not very useful for comparison to the experiment. 

As already discussed in the first part, the GW method can be used to significantly improve the KS eigenvalues. Comparing the band gap of 1.85 eV predicted by PBEsol with the experimental value of 3.3 eV, we find a big discrepancy. Here, we will use the periodic G0W0 method to improve the predictive power of theory[^1]. 

!!! Warning "Warning: periodic GW calculations are expensive"

    Similar to the calculation of the RPA energy, the G0W0 computation is computationally very demanding. A Tier-1 calculation for TiO2 with 4x4x6 k-point grid will require at least about 100 CPUs. Use the output files, which we have provided for you here: <https://gitlab.com/FHI-aims-club/tutorials/rpa-and-gw-for-molecules-and-solids/-/tree/main/Tutorials/previous-version/210716_2/Part-3/solutions/GW>

## Requesting a periodic G₀W₀ calculation

In FHI-aims, a G0W0 calculation can be triggered with the following settings in the `control.in` file:

```
xc                pbesol
relativistic      atomic_zora scalar
qpe_calc          gw_expt
frequency_points  20
anacon_type       0
k_grid            4 4 6
output            band   0.00000  0.00000  0.00000   0.00000  0.50000  0.00000   11 G  X

[attach tight species defaults for Ti and O]
```

This input file will trigger the following calculation: First, a normal DFT calculation with the PBEsol functional will be run. After the SCF cycle has converged the GW calculation will start. This is triggered by the keyword `qpe_calc gw_expt` (the argument `gw_expt` shall indicate that this is a rather new feature (experimental) in FHI-aims and should be checked seriously). The keyword `anacon_type` specifies type of analytical continuation for the self-energy. The argument `0` uses the two-pole approximation.

In addition, add the *tight* species defaults.

!!! Warning "Warning: Request of band output mandatory!"

    It is necessary to request the band output to obtain the GW eigenvalues. At the moment of writing this tutorial, you will get no GW output, if you don't request `output band`.

For the `geometry.in` file we will use the structure of rutile TiO2 optimized with the PBEsol functional from the part 2 of this tutorial:
```
lattice_vector      4.59011462     -0.00000000      0.00000000
lattice_vector     -0.00000000      4.59011462      0.00000000
lattice_vector      0.00000000     -0.00000000      2.93865021
atom_frac       0.30447192      0.30447193      0.00000000 O
atom_frac       0.69552808      0.69552807      0.00000000 O
atom_frac       0.19552808      0.80447193      0.50000000 O
atom_frac       0.80447192      0.19552807      0.50000000 O
atom_frac      -0.00000000      0.00000000     -0.00000000 Ti
atom_frac       0.50000000      0.50000000      0.50000000 Ti
``` 

Again, to obtain reliable=converged GW band structures, there are three points that we have to address to study the convergence behavior:

1. Convergence with the number of frequency points.
2. Convergence with the number of k-points.
3. Convergence with the number of basis functions.


## Convergence with the number of frequency points

Similar to the non-periodic systems, it is also needed to check the convergence of the frequency integration. Here, we will study the convergence of the frequency for a 2x2x3 k-grid and tight species settings, but only using additional basis functions from *tier 1*. For that, comment out the basis functions from tier 2 of the oxygen atom.

As can be seen from the below figure, the direct gap ($\Gamma$-$\Gamma$) depends only moderately on the number of frequency points for our case. For a production calculation, 40 or 80 frequency points would be good choice, but always test this parameter for your calculations. However, throughout the remaining part of this tutorial, we will use only 20 frequency points to save some computational resources.

<!-- ![](solutions/GW-frequency-convergence.png) -->

## Convergence with the number of k-points

Here, we will study the convergence behavior with the number of k-points for tight, tier 1 species defaults (cf. with previous section) using 20 frequency points. 

<!-- ![](solutions/GW-k-grid-convergence.png) -->

We find that the direct band gap depends critically on the number of points. The band gap is by far not converged for 6x6x9 k-points.

## Convergence with the number of basis functions (Update figure)

Here, we will study the convergence behavior with the number of basis functions for the 2x2x3 k-point grid (cf. with previous section) using 20 frequency points. 

<!-- ![](solutions/GapsTiers.png) -->

## G0W0 vs. PBEsol band structure (Update figure)

Note that for this final plot we did not use converged value (tight, tier 1, 4x4x6 k_grid, 60 frequency points). However, we want to discuss the qualitative result. We find a significantly increased band gap for the G0W0@PBEsol band structure of 3.51 eV compared to the PBEsol result of 1.80 eV, which is much closer to the experimentally measured value of 3.3 eV. 

<!-- ![](solutions/bands.png) -->

[^1]:
  Ren, Xinguo, et al. "All-electron periodic G0W0 implementation with numerical atomic orbital basis functions: Algorithm and benchmarks." Physical Review Materials 5.1 (2021): 013807.