#########################################################################################
#
#  Template control.in for total energy calculations of C_2H_4
#
#########################################################################################
#
#  Physical model
  xc                    pbe
  qpe_calc              gw
  anacon_type           1
  n_anacon_par 128
  frequency_points 400
  print_self_energy 6
 
# The original GW100 benchmark results are entirely non-relativistic 
  relativistic          none

################################################################################
#
#  FHI-aims code project
# Igor Ying Zhang and Xinguo Ren, Fritz Haber Institute Berlin, 2012
#
#  Suggested "def2-QZVP" defaults for O atom (to be pasted into control.in file)
#
################################################################################
  species        O
#     global species definitions
    nucleus             8
    mass                15.9994
#
    l_hartree           6
#
    cut_pot             4.0  2.0  1.0
    basis_dep_cutoff    0e-0
#
    radial_base         36 7.0
    radial_multiplier   6
    angular_grids       specified
      division   0.1817   50
      division   0.3417  110
      division   0.4949  194
      division   0.6251  302
      division   0.8014  434
      division   0.8507  590
#      division   0.8762  770
#      division   0.9023  974
#      division   1.2339 1202
#      outer_grid 974
      outer_grid 770
#      outer_grid  434
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      2  s   2.
    valence      2  p   4.
#     ion occupancy
    ion_occ      2  s   1.
    ion_occ      2  p   3.
################################################################################
#
#   For exact comparison with all GTO-based codes, one or more of
#   the following flags are needed:
#
    include_min_basis   .false.
    pure_gauss          .true.
#


# O def2-QZVP
 gaussian 0 8
    116506.4690800            0.0000404  
     17504.3497240            0.0003126  
      3993.4513230            0.0016341  
      1133.0063186            0.0068283  
       369.9956959            0.0241244  
       133.6207435            0.0727302  
        52.0356436            0.1793443  
        21.4619393            0.3305959  
 gaussian 0 2
        89.8350513            0.0964687  
        26.4280108            0.9411748  
 gaussian 0 1 9.2822824649
 gaussian 0 1 4.0947728533
 gaussian 0 1 1.3255349078
 gaussian 0 1 0.51877230787
 gaussian 0 1 0.19772676454
 gaussian 1 5
       191.1525581            0.0025116  
        45.2333567            0.0200392  
        14.3534659            0.0936091  
         5.2422372            0.3061813  
         2.0792419            0.6781050  
 gaussian 1 1 0.84282371424
 gaussian 1 1 0.33617694891
 gaussian 1 1 0.12863997974
 gaussian 2 1 3.77500000
 gaussian 2 1 1.30000000
 gaussian 2 1 0.44400000
 gaussian 3 1 2.66600000
 gaussian 3 1 0.85900000
 gaussian 4 1 1.84600000
################################################################################
#
#  FHI-aims code project
# Igor Ying Zhang and Xinguo Ren, Fritz Haber Institute Berlin, 2012
#
#  Suggested "def2-QZVP" defaults for B atom (to be pasted into control.in file)
#
################################################################################
  species        B
#     global species definitions
    nucleus             5
    mass                10.811
#
    l_hartree           6
#
    cut_pot             4.0  2.0  1.0
    basis_dep_cutoff    0e-0
#
    radial_base         32 7.0
    radial_multiplier   6
    angular_grids       specified
      division   0.3742  110
      division   0.5197  194
      division   0.5753  302
      division   0.7664  434
#      division   0.8392  770
#      division   1.6522  974
#      outer_grid   974
      outer_grid  770
#      outer_grid  434
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      2  s   2.
    valence      2  p   1.
#     ion occupancy
    ion_occ      2  s   1.
################################################################################
#
#   For exact comparison with all GTO-based codes, one or more of
#   the following flags are needed:
#
    include_min_basis   .false.
    pure_gauss          .true.
#


# B def2-QZVP
 gaussian 0 8
     46447.6670560            0.0000384  
      6957.6889042            0.0002984  
      1583.4428403            0.0015645  
       448.4660101            0.0065477  
       146.2863926            0.0231390  
        52.7843861            0.0696158  
        20.5193962            0.1711964  
         8.4185659            0.3191319  
 gaussian 0 2
        36.5100183            0.0789906  
        10.5418540            0.7892638  
 gaussian 0 1 3.6004091387
 gaussian 0 1 1.5617023749
 gaussian 0 1 0.44997370775
 gaussian 0 1 0.18075230237
 gaussian 0 1 0.71596696319E-01
 gaussian 1 5
        72.2404628            0.0008654  
        16.8077072            0.0068762  
         5.2259411            0.0309767  
         1.8508351            0.1043236  
         0.7220678            0.2616414  
 gaussian 1 1 0.29491018056
 gaussian 1 1 0.12201141839
 gaussian 1 1 0.49865393625E-01
 gaussian 2 1 1.11000000
 gaussian 2 1 0.40200000
 gaussian 2 1 0.14500000
 gaussian 3 1 0.88200000
 gaussian 3 1 0.31100000
 gaussian 4 1 0.67300000
################################################################################
#
#  FHI-aims code project
# Igor Ying Zhang and Xinguo Ren, Fritz Haber Institute Berlin, 2012
#
#  Suggested "def2-QZVP" defaults for N atom (to be pasted into control.in file)
#
################################################################################
  species        N
#     global species definitions
    nucleus             7
    mass                14.0067
#
    l_hartree           6
#
    cut_pot             4.0  2.0  1.0
    basis_dep_cutoff    0e-0
#
    radial_base         35 7.0
    radial_multiplier   6
    angular_grids       specified
      division   0.1841   50
      division   0.3514  110
      division   0.5126  194
      division   0.6292  302
      division   0.6939  434
      division   0.7396  590
#      division   0.7632  770
#      division   0.8122  974
#      division   1.1604 1202
#      outer_grid  974
      outer_grid  770
#      outer_grid  434
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      2  s   2.
    valence      2  p   3.
#     ion occupancy
    ion_occ      2  s   1.
    ion_occ      2  p   2.
################################################################################
#
#   For exact comparison with all GTO-based codes, one or more of
#   the following flags are needed:
#
    include_min_basis   .false.
    pure_gauss          .true.
#


# N def2-QZVP
 gaussian 0 8
     90726.8892100            0.0000393  
     13590.5288010            0.0003051  
      3092.9883781            0.0016001  
       875.9987636            0.0066983  
       285.7446998            0.0236901  
       103.1191342            0.0714554  
        40.1285568            0.1763277  
        16.5280957            0.3267759  
 gaussian 0 2
        69.3909610            0.0800521  
        20.4282006            0.7826806  
 gaussian 0 1 7.1292587972
 gaussian 0 1 3.1324304893
 gaussian 0 1 0.98755778723
 gaussian 0 1 0.38765721307
 gaussian 0 1 0.14909883075
 gaussian 1 5
       150.0574267           -0.0008622  
        35.4915995           -0.0068571  
        11.2478642           -0.0317957  
         4.0900305           -0.1053740  
         1.6220573           -0.2451971  
 gaussian 1 1 0.66442261530
 gaussian 1 1 0.27099770070
 gaussian 1 1 0.10688749984
 gaussian 2 1 2.83700000
 gaussian 2 1 0.96800000
 gaussian 2 1 0.33500000
 gaussian 3 1 2.02700000
 gaussian 3 1 0.68500000
 gaussian 4 1 1.42700000
