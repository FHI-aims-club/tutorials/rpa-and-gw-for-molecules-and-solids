gnuplot> plot "self_energy_analytic_state_8.dat" u 1:($2*27.211399) with lines


##################### FROM output

 state     occ_num        e_gs        e_x^ex        e_xc^gs        e_c^nloc        e_qp
----------------------------------------------------------------------------------------
       8    2.0000       -6.7356      -16.3378      -13.0778       -0.2012      -10.1968   --> look for this line

gnuplot> f(x) = x - 13.0778 + 16.3378 + 6.7356
gnuplot> replot f(x)
