xc           pbe
qpe_calc     gw
anacon_type  1
override_illconditioning .true.
################################################################################
#
#  FHI-aims code project
# Igor Ying Zhang and Xinguo Ren, Fritz Haber Institute Berlin, 2012
#
#  Suggested "cc-pV6Z" defaults for H atom (to be pasted into control.in file)
#
################################################################################
  species        H
#     global species definitions
    nucleus             1
    mass                1.00794
#
    l_hartree           6
#
    cut_pot             4.0  2.0  1.0
    basis_dep_cutoff    0e-0
#     
    radial_base         24 7.0
    radial_multiplier   6
    angular_grids       specified
      division   0.1930   50
      division   0.3175  110
      division   0.4293  194
      division   0.5066  302
      division   0.5626  434
      division   0.5922  590
#      division   0.6227  974
#      division   0.6868 1202
      outer_grid  770
#      outer_grid  434
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      1  s   1.
#     ion occupancy
    ion_occ      1  s   0.5
################################################################################
#
#   For exact comparison with all GTO-based codes, one or more of
#   the following flags are needed:
#
    include_min_basis   .false.
    pure_gauss          .true.
#


# H cc-pV6Z
 gaussian 0 5
      1776.7755600            0.0000440  
       254.0177120            0.0003720  
        54.6980390            0.0020940  
        15.0183440            0.0088630  
         4.9150780            0.0305400  
 gaussian 0 1 1.79492400
 gaussian 0 1 0.71071600
 gaussian 0 1 0.30480200
 gaussian 0 1 0.13804600
 gaussian 0 1 0.06215700
 gaussian 1 1 8.6490000
 gaussian 1 1 3.4300000
 gaussian 1 1 1.3600000
 gaussian 1 1 0.5390000
 gaussian 1 1 0.2140000
 gaussian 2 1 4.4530000
 gaussian 2 1 1.9580000
 gaussian 2 1 0.8610000
 gaussian 2 1 0.3780000
 gaussian 3 1 4.1000000
 gaussian 3 1 1.7800000
 gaussian 3 1 0.7730000
 gaussian 4 1 3.1990000
 gaussian 4 1 1.3260000
 gaussian 5 1 2.6530000
################################################################################
#
#  FHI-aims code project
# Igor Ying Zhang and Xinguo Ren, Fritz Haber Institute Berlin, 2012
#
#  Suggested "cc-pV6Z" defaults for C atom (to be pasted into control.in file)
#
################################################################################
  species        C
#     global species definitions
    nucleus             6
    mass                12.0107
#
    l_hartree           6
#
    cut_pot             4.0  2.0  1.0
    basis_dep_cutoff    0e-0
#
    radial_base         34 7.0
    radial_multiplier   6
    angular_grids       specified
      division   0.2187   50
      division   0.4416  110
      division   0.6335  194
      division   0.7727  302
      division   0.8772  434
      division   0.9334  590
#      division   0.9924  770
#      division   1.0230  974
#      division   1.5020 1202
#     outer_grid  974
      outer_grid  770
#      outer_grid  434
################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      2  s   2.
    valence      2  p   2.
#     ion occupancy
    ion_occ      2  s   1.
    ion_occ      2  p   1.
################################################################################
#
#   For exact comparison with all GTO-based codes, one or more of
#   the following flags are needed:
#
    include_min_basis   .false.
    pure_gauss          .true.
#


# C cc-pV6Z
 gaussian 0 11
    312100.0000000            0.0000057  
     46740.0000000            0.0000441  
     10640.0000000            0.0002319  
      3013.0000000            0.0009790  
       982.8000000            0.0035516  
       354.8000000            0.0114406  
       138.4000000            0.0329986  
        57.3500000            0.0840535  
        24.9200000            0.1806761  
        11.2300000            0.3049114  
         5.2010000            0.3414157  
 gaussian 0 11
    312100.0000000           -0.0000012  
     46740.0000000           -0.0000094  
     10640.0000000           -0.0000495  
      3013.0000000           -0.0002086  
       982.8000000           -0.0007602  
       354.8000000           -0.0024547  
       138.4000000           -0.0072015  
        57.3500000           -0.0188074  
        24.9200000           -0.0432500  
        11.2300000           -0.0825973  
         5.2010000           -0.1285759  
 gaussian 0 1 2.4260000
 gaussian 0 1 0.9673000
 gaussian 0 1 0.4456000
 gaussian 0 1 0.1971000
 gaussian 0 1 0.0863500
 gaussian 1 5
       295.2000000            0.0001425  
        69.9800000            0.0012201  
        22.6400000            0.0063370  
         8.4850000            0.0235188  
         3.4590000            0.0699045  
 gaussian 1 1 1.5040000
 gaussian 1 1 0.6783000
 gaussian 1 1 0.3087000
 gaussian 1 1 0.1400000
 gaussian 1 1 0.0617800
 gaussian 2 1 4.5420000
 gaussian 2 1 1.9790000
 gaussian 2 1 0.8621000
 gaussian 2 1 0.3756000
 gaussian 2 1 0.1636000
 gaussian 3 1 2.6310000
 gaussian 3 1 1.2550000
 gaussian 3 1 0.5988000
 gaussian 3 1 0.2857000
 gaussian 4 1 2.6520000
 gaussian 4 1 1.2040000
 gaussian 4 1 0.5470000
 gaussian 5 1 2.0300000
 gaussian 5 1 0.8511000
 gaussian 6 1 1.4910000
