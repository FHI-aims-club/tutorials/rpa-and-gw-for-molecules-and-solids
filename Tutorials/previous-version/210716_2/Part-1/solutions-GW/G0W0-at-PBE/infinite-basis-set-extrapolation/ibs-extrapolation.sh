dirs="DZ TZ QZ 5Z 6Z"

for d in $dirs
do
  mkdir $d
  cp geometry.in $d/geometry.in
  cp control.in $d/control.in
  cat ~/software/FHIaims/species_defaults/non-standard/gaussian_tight_770/cc-pV$d/01_H_default >> $d/control.in
  cat ~/software/FHIaims/species_defaults/non-standard/gaussian_tight_770/cc-pV$d/06_C_default >> $d/control.in
done
