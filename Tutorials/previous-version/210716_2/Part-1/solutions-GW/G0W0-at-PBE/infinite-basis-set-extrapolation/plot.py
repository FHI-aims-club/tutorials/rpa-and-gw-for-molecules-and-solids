from matplotlib import pyplot as plt
import numpy as np

params = {'mathtext.default': 'regular' }          
plt.rcParams.update(params)

data = np.loadtxt('qp-levels.dat')
x = 1/data[:,0]
y = data[:,1]
correlation_matrix = np.corrcoef(x, y)
correlation_xy = correlation_matrix[0,1]
r_squared = correlation_xy**2
print(r_squared)

m,b = np.polyfit(x, y, 1)
plt.plot([0,0.01],[b,m*0.01+b],label='linear fit')
plt.plot(1/data[:,0],data[:,1],"o",label='G0W0@PBE')
plt.xlabel('1/N$_{basis}$')
plt.ylabel('HOMO level (eV)')
plt.title('C$_2$H$_4$, G0W0@PBE')
plt.legend()
plt.show()

