import numpy as np
from matplotlib import pyplot as plt


data = np.loadtxt('convergence-HOMO.dat')
fig, (ax1, ax2) = plt.subplots(1, 2)

ax1.plot(data[:,0],data[:,1])
plt.setp(ax1, xticks=np.arange(3)+1, xticklabels=[1,2,3])
ax1.set_title('PBE')
ax1.set_xlabel("tier")
ax1.set_ylabel("Highest occupied state (eV)")
ax2.plot(data[:,0],data[:,2])
plt.setp(ax2, xticks=np.arange(3)+1, xticklabels=[1,2,3])
ax2.set_xlabel("tier")
ax2.set_title('G0W0@PBE')
plt.show()
