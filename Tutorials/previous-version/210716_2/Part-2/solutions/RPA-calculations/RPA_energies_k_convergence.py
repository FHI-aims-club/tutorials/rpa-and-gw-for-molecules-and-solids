from pathlib import Path
from matplotlib import pyplot as plt
import numpy as np

p = Path(".")

k_grids = [
    "k_grid_2x2x3",
    "k_grid_4x4x6",
    "k_grid_6x6x9",
]
k_labels = ["2x2x3", "4x4x6", "6x6x9"]
species_defaults = [
    "light",
    # "intermediate",
    # "tight",
    # "tier2",
]
geometries = ["perfect", "distorted"]
energies = {}

# Parse the RPA and DFT total energies
for geometry in geometries:
    energies[geometry] = {}
    for species in species_defaults:
        dft_and_rpa = {"rpa": [], "dft": []}
        for k_grid in k_grids:
            output = p / geometry / species / k_grid / "aims.out"
            try:
                with open(output) as f:
                    lines = f.readlines()
                for line in lines:
                    if line.startswith("    DFT/HF total energy"):
                        e = float(line.split()[6])
                        dft_and_rpa["dft"].append(e)
                    if line.startswith("    RPA total energy"):
                        e = float(line.split()[6])
                        dft_and_rpa["rpa"].append(e)
            except FileNotFoundError:
                print('No FHI-aims output file "aims.out" found.')
                print("Skipping this folder:", output)

        energies[geometry][species] = dft_and_rpa

# Plot the Energy Differences
plt.rcParams.update({"font.size": 20})
fig, (ax1, ax2) = plt.subplots(1, 2)


for species in species_defaults:
    k_len = len(energies[geometry][species]["rpa"])
    x = np.arange(k_len)
    rpa_diff = np.zeros(k_len)
    dft_diff = np.zeros(k_len)
    for i, geometry in enumerate(geometries):
        etot_rpa = np.array(energies[geometry][species]["rpa"])
        etot_dft = np.array(energies[geometry][species]["dft"])
        ax2.plot(x, etot_rpa - etot_rpa[0], label=f"RPA ({geometry}, {species})", marker="o")

        rpa_diff += (-1) ** i * etot_rpa
        dft_diff += (-1) ** i * etot_dft

    ax1.plot(x, rpa_diff, label=f"RPA ({species})", marker="o")
    ax1.plot(x, dft_diff, label=f"PBE ({species})", marker="o")

fig.suptitle("Stability of rutile TiO2: k-point convergence")

ax1.legend()
plt.setp(ax1, xticks=np.arange(3), xticklabels=k_labels)
ax1.set_xlabel("k-grid")
ax1.set_ylabel("Energy difference perfect-distorted (eV)")

ax2.legend()
plt.setp(ax2, xticks=np.arange(3), xticklabels=k_labels)
ax2.set_xlabel("k-grid")
ax2.set_ylabel("Total energy (eV)")

plt.show()
