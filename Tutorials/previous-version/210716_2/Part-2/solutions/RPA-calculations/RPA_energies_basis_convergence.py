from pathlib import Path
from matplotlib import pyplot as plt
import numpy as np

p = Path(".")

k_grids = [
    "k_grid_2x2x3",
    "k_grid_4x4x6",
    "k_grid_6x6x9",
]
k_labels = ["2x2x3", "4x4x6", "6x6x9"]
species_defaults = [
    "light",
    "intermediate",
    "tight",
    "tier2",
]
geometries = ["perfect", "distorted"]
energies = {}

# Parse the RPA and DFT total energies
for geometry in geometries:
    energies[geometry] = {}
    for species in species_defaults:
        dft_and_rpa = {"rpa": [], "dft": []}
        for k_grid in k_grids:
            output = p / geometry / species / k_grid / "aims.out"
            try:
                with open(output) as f:
                    lines = f.readlines()
                for line in lines:
                    if line.startswith("    DFT/HF total energy"):
                        e = float(line.split()[6])
                        dft_and_rpa["dft"].append(e)
                    if line.startswith("    RPA total energy"):
                        e = float(line.split()[6])
                        dft_and_rpa["rpa"].append(e)
            except FileNotFoundError:
                print('No FHI-aims output file "aims.out" found.')
                print("Skipping this folder:", output)

        energies[geometry][species] = dft_and_rpa


plt.rcParams.update({"font.size": 20})
k_len = 3
for i in range(k_len):
    rpa = []
    dft = []
    for species in species_defaults:
        try:
            rpa.append(
                energies["perfect"][species]["rpa"][i]
                - energies["distorted"][species]["rpa"][i]
            )
            dft.append(
                energies["perfect"][species]["dft"][i]
                - energies["distorted"][species]["dft"][i]
            )
        except:
            pass
    print(rpa)
    plt.plot(np.array(rpa), marker="o", label=f"RPA, {k_labels[i]}")
    plt.plot(np.array(dft), marker="o", label=f"PBE, {k_labels[i]}")


plt.legend()
plt.title("RPA Energy differences")
plt.xticks(np.arange(4), ["light", "intermediate", "tight", "tier2"])
plt.xlabel("species defaults")
plt.ylabel("Energy difference perfect-distorted (eV)")
plt.show()
