# Welcome to the RPA, GW, and BSE tutorial!

In this tutorial, you will learn about three methods that go beyond DFT: the many-body perturbation theory in the GW approach, the random-phase approximation (RPA), and the Bethe-Salpeter equation (BSE). These methods help to improve the predictive power of the numerical simulations, but at the expense of much higher computational costs.

**This tutorial is made for FHI-aims version 210716_2**. The updated version of this tutorial can be found [here](../../index.md). This tutorial was a joint work by (in alphabetical order) Volker Blum, Saeed Bohloul, Dorothea Golze, Levi Keller, Sebastian Kokott, Mohammad Nakhaee, Xinguo Ren, and Yi Yao. 

## The objective

This tutorial introduces several methods that go beyond the DFT level. The following points will be addressed: 

* How to run GW, RPA and BSE calculations for solids and molecules with FHI-aims.
* How to interpret the results of the simulations and their meaning in the context of theoretical spectroscopy (e.g. photoemission and core level spectroscopy)
* Learn about the reference method RPA for benchmarking local and semi-local exchange-correlation functionals.


## Prerequisites

Users of this tutorial should have a sufficiently good understanding:

* About the basics of running FHI-aims. If this is not the case, please consider to study the [Basics of Running FHI-aims tutorial](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/) first.
* Some of the exercises are computationally very demanding and may require a node or a whole bunch of nodes from computer clusters. Those parts cannot be run on a laptop and are specifically marked. Nevertheless, we still recommend to go through theses expensive exercises. We provide the complete input and output files, which you can use to familiarize with these methods. To download all solutions, it is easiest to clone this repository: <https://gitlab.com/FHI-aims-club/tutorials/rpa-and-gw-for-molecules-and-solids>

        git clone https://gitlab.com/FHI-aims-club/tutorials/rpa-and-gw-for-molecules-and-solids

## Useful links

- Tutorials gitlab repository, which contains all the documents and simulation data: <https://gitlab.com/FHI-aims-club/tutorials/rpa-and-gw-for-molecules-and-solids> 
- Utilities gitlab repository, particularly [CLIMS](https://gitlab.com/FHI-aims-club/utilities/clims): <https://gitlab.com/FHI-aims-club/utilities>
- Graphical Interface for Materials Simulation (GIMS): <https://gims.ms1p.org>


## Outline

1. GW and BSE calculations for molecules
    1. GW eigenvalues and Photoemission
    2. Counterpoise Correction
    3. Core levels
    4. BSE
    5. Multiple GW solutions
2. RPA for solids
3. GW for solids


