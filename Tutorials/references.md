# References

Beside the [references from the first](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/references/) tutorial about the basics of FHI-aims, a lot of effort has been invested to also move forward with the beyond-DFT part of FHI-aims. Here is a short list of publications of the implementations that have been referenced in this tutorial:

* Basics of handling the two-electron Coulomb operator for exact exchange and correlated methods (GW, RPA) in FHI-aims:

Xinguo Ren, Patrick Rinke, Volker Blum, Jürgen Wieferink, Alex Tkatchenko, Andrea Sanfilippo, Karsten Reuter, and Matthias Scheffler, Resolution-of-identity approach to Hartree-Fock, hybrid density functionals, RPA, MP2, and GW with numeric atom-centered orbital basis functions. New Journal of Physics 14, 053020 (2012).

* Periodic GW for quasiparticle excitations:

Xinguo Ren, Florian Merz, Hong Jiang, Yi Yao, Markus Rampp, Hermann Lederer, Volker Blum and Matthias Scheffler. All-electron periodic G0W0 implementation with numerical atomic orbital basis functions: algorithm and benchmarks. Physical Review Materials 5, 013807 (2021). https://doi.org/10.1103/PhysRevMaterials.5.013807

* Bethe-Salpeter Equation for neutral excitations in molecules:

Chi Liu, Jan Kloppenburg, Yi Yao, Xinguo Ren, Heiko Appel, Yosuke Kanai, Volker Blum. All-electron ab initio Bethe-Salpeter equation approach to neutral excitations in molecules with numeric atom-centered orbitals. The Journal of Chemical Physics, 152, 044105 (2020). 

* Bethe-Salpeter Equation for neutral excitations in periodic systems:

Ruiyi Zhou, Yao Yi, Volker Blum, Xinguo Ren, Yosuke Kanai. All-electron $ BSE@ GW $ method with Numeric Atom-Centered Orbitals for Extended Systems. arXiv preprint arXiv:2406.11122 (2024).

* Valence Correlation-consistent NAOs:

Zhang, Igor Ying, et al. "Numeric atom-centered-orbital basis sets with valence-correlation consistency from H to Ar." New Journal of Physics 15.12 (2013): 123033.