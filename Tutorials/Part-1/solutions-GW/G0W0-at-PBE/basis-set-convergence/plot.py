import re
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

dft_homos = []
qp_homos = []
tiers = [1, 2, 3]
for tier in tiers:
    out_lines = open(f"tier-{tier}/aims.out").read()
    dft_homo = re.search(r"DFT/Hartree-Fock HOMO level \(eV\): +([-.\d]*) +([-.\d]*)", out_lines, re.MULTILINE).group(1)
    dft_homos.append(float(dft_homo))
    qp_homo = re.search(r"Quasiparticle HOMO level \(eV\): +([-.\d]*) +([-.\d]*)", out_lines, re.MULTILINE).group(1)
    qp_homos.append(float(qp_homo))

fig, (ax1, ax2) = plt.subplots(1, 2)

ax1.plot(tiers, dft_homos, '-o')
ax1.xaxis.set_major_locator(MaxNLocator(integer=True))
ax1.set_title('PBE')
ax1.set_xlabel("tier")
ax1.set_ylabel("Highest occupied state (eV)")
ax2.plot(tiers, qp_homos, '-o')
ax2.xaxis.set_major_locator(MaxNLocator(integer=True))
ax2.set_xlabel("tier")
ax2.set_title('G0W0@PBE')
plt.show()
