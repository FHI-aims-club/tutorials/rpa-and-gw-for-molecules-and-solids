#!/usr/bin/python3
"""
If called with an FHI-aims outputfile from a G0W0 calculation as argument, this
script will extract the quasi-particle energies and convert them into a spectrum
by means of broadening.
"""
import sys
import math
import re

# broad  = 0.1
# intens = 1.000
##Definition of an auxiliary grid to great
##spectrum range
# Smin=-35 #eV
# Smax=-6  #eV
# spacing=0.02 #grid interval


def fatal(msg):
    """fail gracefully without intimidating stacktrace"""
    print("\x1b[31mFATAL ERROR:\x1b[0m", msg, "\x1b[0m")
    sys.exit(1)


class GridSpec(object):  # pylint:disable=R0903
    """a simple wrapper for a linear gridspec (no numpy at target available)"""

    def __init__(self, minval, maxval, spacing):
        self._min = minval
        self._max = maxval
        self._space = spacing
        # skip sanity checks because this class does not take user input, unless
        # called interactively, then they should know what they do

    def __iter__(self):
        return self._griditerator()

    def _griditerator(self):
        """an iterator over the gridpoints contained in this GridSpec"""
        pos = self._min
        while pos <= self._max:
            yield pos
            pos += self._space


def read_qp_energies(filename):
    """
    extract the QP-energies from an aims-outputfile and return them as a list
    """
    regex = re.compile(r"([0-9]+)[ ]+[.0-9]+(?:[ ]+[-.0-9]+){4}[ ]+([-.0-9]+)")
    tablestart = False
    states = list()
    with open(filename, "r") as infile:
        for line in infile:
            if not tablestart:
                tablestart = "GW quasi-particle energy levels" in line
                if tablestart:
                    for i in range(6):  # pylint:disable=W0612
                        infile.readline()
            else:
                if len(states) > 0 and "--------------------------" in line:
                    break
                # elif "--------------------------" in line or line == " \n":
                #    continue
                else:
                    match = regex.search(line)
                    if match == None:
                        fatal("Table row did not match extraction pattern: " + "'%s'" % line)
                    elif int(match.group(1)) != len(states) + 1:
                        fatal(
                            "Table row did not match the expected state "
                            + "index %i: '%s'" % (len(states) + 1, line)
                        )
                    states.append(float(match.group(2)))
    if not tablestart or len(states) == 0:
        fatal("Could not find G0W0 quasi-particle energies in the given file!")
    return states


def make_spectrum(gridspec, states, broadening=0.1):
    """generate a spectrum by broadening the given states on the given grid"""
    for point in gridspec:
        value = 0.0
        for peak in states:
            value += math.exp(-1.0 / broadening * (point - peak) ** 2)
        print(point, value)


def main():
    """
    take the aims-outfile given as argument and try to create a G0W0 spectrum
    for the range (-35, 0) eV on a 0.02 eV grid
    """
    try:
        grid = GridSpec(-35, 0, 0.02)
        states = read_qp_energies(sys.argv[1])
        make_spectrum(grid, states)
    except Exception as error:  # pylint:disable=W0703
        # if something fails, fail gracefully and hide the stacktrace
        fatal(str(error))


if __name__ == "__main__":
    main()
