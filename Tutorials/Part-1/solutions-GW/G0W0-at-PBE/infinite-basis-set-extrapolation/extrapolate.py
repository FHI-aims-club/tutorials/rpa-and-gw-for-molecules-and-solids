#!/usr/bin/env python

# This script builds the infinite basis set extrapolation for
# methane HOMO quasi-particle level

import re
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt


dirs = ['DZ', 'TZ', 'QZ', '5Z', '6Z']
output_file_name = 'aims.out'

nums_basis = []
qp_homos = []
for dir in dirs:
    out_lines = open(Path(dir) / output_file_name).read()
    num_basis = re.search(r"Total number of basis functions : +([-.\d]*)", out_lines, re.MULTILINE).group(1)
    nums_basis.append(int(num_basis))
    qp_homo = re.search(r"Quasiparticle HOMO level \(eV\): +([-.\d]*) +([-.\d]*)", out_lines, re.MULTILINE).group(1)
    qp_homos.append(float(qp_homo))

nums_basis = np.array(nums_basis)    
qp_homos = np.array(qp_homos)

homos_fit = np.poly1d(np.polyfit(1 / nums_basis[::-1], qp_homos[::-1], 1))

plt.title(r'C$_2$H$_4$, G$_0$W$_0$@PBE')
plt.xlabel(r'1/N$_{basis}$')
plt.ylabel('HOMO level (eV)')
plt.plot(1. / nums_basis, qp_homos, 'o', label=r'G$_0$W$_0$@PBE')
x = np.linspace(0., 0.025, 6)
plt.plot(x, homos_fit(x), '-', label='Linear fit')
plt.legend()
plt.annotate(f'Infinite basis set extrapolation: {homos_fit(0):.2f} eV',
             color='red',
             xy=(0., homos_fit(0)), 
             xytext=(0.4, 0.3), 
             textcoords='figure fraction', 
             arrowprops=dict(arrowstyle="->", 
                             connectionstyle="arc3,rad=-0.2", 
                             color="r"))

plt.savefig('ibs-extrapolation.png')
plt.show()