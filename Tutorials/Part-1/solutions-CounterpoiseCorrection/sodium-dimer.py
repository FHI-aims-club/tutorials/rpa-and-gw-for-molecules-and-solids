from ase.io import write
from ase.atoms import Atoms
from ase.calculators.aims import Aims
from matplotlib import pyplot as plt
from pathlib import Path
import subprocess


def parse_rpa_dft_energy(outfile):
    with open(outfile, "r") as fd:
        line = next(l for l in fd if "    DFT/HF total energy" in l)
        dft = float(line.split()[6])

        line = next(l for l in fd if "    RPA total energy" in l)
        rpa = float(line.split()[6])
    return rpa, dft


def run(calc_name, atoms, directory, sname, calc_defaults, counterpoise=False):
    sdir = species_dir[sname]
    print(f"{calc_name}, species {sname}")
    calc = Aims(
        **calc_defaults,
        species_dir=sdir,
        directory=directory,
    )
    atoms.calc = calc
    atoms.calc.write_input(atoms, geo_constrain=False)
    if counterpoise:
        with open(directory / "geometry.in") as fd:
            lines = fd.readlines()
        with open(directory / "geometry.in", "w") as fd:
            fd.write("empty 0.00000000 0.00000000 0.00000000 Na\n")
            fd.write(lines[6])
            if lines[7]:
                fd.write(lines[7])
    proc = subprocess.Popen(command, shell=True, cwd=directory)
    errorcode = proc.wait()
    print(f"-- Calculation Finished")


def run_all():
    for i, (sname, sdir) in enumerate(species_dir.items()):
        atom_dir = p / sname / "atom"
        run("Na atom", Na_atom, atom_dir, sname, calc_defaults_atom)

        for pos in Na_position:
            dimer_dir = p / sname / "raw" / str(pos)
            dimer_dir_cp = p / sname / "counterpoise" / str(pos)
            dimer.positions[1, 2] = pos
            run(
                f"Na dimer, distance {pos} AA, cpc",
                dimer,
                dimer_dir_cp,
                sname,
                calc_defaults_atom,
                counterpoise=True,
            )
            run(f"Na dimer, distance {pos} AA", dimer, dimer_dir, sname, calc_defaults_dimer)


def plot_all():
    plt.figure(figsize=(4.8,6.4))
    bond_curves = {}
    for i, (sname, sdir) in enumerate(species_dir.items()):
        bond_curves[sname] = {
            "dft": {"raw": [], "counterpoise": [], "atom": None},
            "rpa": {"raw": [], "counterpoise": [], "atom": None},
        }
        atom_dir = p / sname / "atom"
        rpa, dft = parse_rpa_dft_energy(atom_dir / "aims.out")
        bond_curves[sname]["rpa"]["atom"] = rpa
        bond_curves[sname]["dft"]["atom"] = dft
        for pos in Na_position:
            dimer_dir = p / sname / "raw" / str(pos)
            dimer_dir_cp = p / sname / "counterpoise" / str(pos)
            rpa_cp, dft_cp = parse_rpa_dft_energy(dimer_dir_cp / "aims.out")
            rpa, dft = parse_rpa_dft_energy(dimer_dir / "aims.out")
            raw = rpa - 2 * bond_curves[sname]["rpa"]["atom"]
            counterpoise = rpa - 2 * rpa_cp
            bond_curves[sname]["rpa"]["raw"].append(raw)
            bond_curves[sname]["rpa"]["counterpoise"].append(counterpoise)

        print(f"Plotting {sname} results")
        plt.plot(
            Na_position,
            bond_curves[sname]["rpa"]["raw"],
            label=f"RPA, {sname}, raw",
            color=colors[i],
            linestyle="dashed",
        )
        plt.plot(
            Na_position,
            bond_curves[sname]["rpa"]["counterpoise"],
            label=f"RPA, {sname}, cpc",
            color=colors[i],
        )
    plt.title("Na dimer, counterpoise correction")
    plt.xlabel("Na distance (Angstrom)")
    plt.ylabel("Binding Energy (eV)")
    plt.legend()
    plt.savefig('rpa-cpc.png', bbox_inches='tight')
    plt.show()


p = Path(".")
aims_dir = Path.home() / 'workspace' / 'FHIaims'

colors = ["tab:blue", "tab:orange", "tab:green", "tab:purple"]

species_dir = {
    "light": (aims_dir / "species_defaults/defaults_2020/light").as_posix(),
    "tight": (aims_dir / "species_defaults/defaults_2020/tight").as_posix(),
    "cc-pV5Z": (aims_dir / "species_defaults/non-standard/gaussian_tight_770/cc-pV5Z").as_posix(),
    "NAO-VCC-5Z": (aims_dir / "species_defaults/NAO-VCC-nZ/NAO-VCC-5Z").as_posix(),
}

calc_defaults_dimer = {
    "xc": "pbe",
    "relativistic": "atomic_zora scalar",
    "total_energy_method": "rpa",
    "override_illconditioning": ".true.",
}

calc_defaults_atom = {
    "xc": "pbe",
    "spin": "collinear",
    "relativistic": "atomic_zora scalar",
    "total_energy_method": "rpa",
    "override_illconditioning": ".true.",
}

command = "mpirun -np 6 aims.x > aims.out"

Na_position = [2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4, 3.6]

dimer = Atoms("Na2", positions=([0, 0, 0], [0, 0, 2]), magmoms=[0, 1])
Na_atom = Atoms("Na", positions=([0, 0, 0],), magmoms=[1])

#run_all()
plot_all()
