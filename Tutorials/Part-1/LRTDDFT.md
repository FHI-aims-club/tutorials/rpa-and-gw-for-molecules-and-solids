# LRTDDFT for Molecules

In this tutorial we will show the reader how to perform the Linear Response TDDFT (LRTDDFT) calculation on molecules using FHI-aims to study the neutral excitations. 
The implementation of the LRTDDFT in FHI-aims based on NAO are described by Liu, et al. [^1]

## Background

The same as BSE, the physical process described by the LRTDDFT equation is the charge neutral optical excitation as shown in this figure.

<a name="optical_excitation"></a>
![optical_absorption_figure](../img/optical_absorption.png)

Figure from Golze et al. [^2]

The systems can absorb energy at the frequencies of transitions to excited states.  The so-called Casida equation is derived by formulating the polarizability as a sum over states of the many-body system. In 1995, Casida derived that the square of excitation energies can be obtained as an eigenvalue problem in the matrix form:

$$
\mathbf{C} \mathbf{F_s}=\Omega^2 \mathbf{F_s}.
$$

Here, the $\mathbf{C}$ is the so-called Casida matrix, which has the same dimension of $\mathbf{A}$ and $\mathbf{B}$ in the BSE.
$\Omega$ are the neutral many-body excitation energies. $\mathbf{F_s}$ are the associated eigenvectors and can be related to the oscillator strengths via the dipole operator[^3]. Here, the TDA was adapted as it is widely used for TDDFT.
The $\mathbf{C}$ matrix can be written in the basis of product of the (g)KS orbitals
$$
C_{i a, j b}(\omega)=\delta_{i, j} \delta_{a, b}\left(\epsilon_a-\epsilon_i\right)^2+2 \sqrt{\left(\epsilon_a-\epsilon_i\right)} K_{i a, j b}(\omega) \sqrt{\left(\epsilon_b-\epsilon_j\right)},
$$
where $\epsilon_a$, $\epsilon_i$ are KS eigenvalues, the kernel $K_{i a, j b}$ is defined as
$$
K_{i a, j b}(\omega)= \iint \psi_i^{\dagger}(\mathbf{r}) \psi_a(\mathbf{r})\left[ \frac{1}{\left|\mathbf{r}-\mathbf{r}^{\prime}\right|}
+f_{\mathrm{xc}}\left[n_0\right]\left(\mathbf{r}, \mathbf{r}^{\prime}, \omega\right)\right] \psi_j\left(\mathbf{r}^{\prime}\right) \psi_b^{\dagger}\left(\mathbf{r}^{\prime}\right) d \mathbf{r} d \mathbf{r}^{\prime}
$$
where $n_0$ is the (g)KS ground state electron density and $f_{xc}[n_0]$ is the exchange-correlation (XC) kernel, which is a functional derivative that describes how the exchange-correlation potential changes with respect to the electron density. [^4] Within FHI-aims, we currently have the LDA XC kernel implemented with the adiabatic approximation (i.e. $f_{\mathrm{xc}}\left(\mathbf{r}, \mathbf{r}^{\prime}, \omega\right) = f_{\mathrm{xc}}\left(\mathbf{r}, \mathbf{r}^{\prime}\right)$), and the implementation works only for isolated (non-periodic) systems.  


## Exercise

In this LRTDDFT exercise, you will perform a LRTDDFT calculation based on LDA(VWN) exchange-correlation functional. Tier 2 + aug2 basis set for ethylene C$_2$H$_4$ is used.
The quantities of interest in this case are the LRTDDFT eigenvalues. You can proceed as follows:

* Generate the `geometry.in` file for ethylene from the experimental data (extracted from [here](https://cccbdb.nist.gov)):

```
atom	0.0000	0.0000	0.6695 C
atom	0.0000	0.0000	-0.6695 C
atom	0.0000	0.9289	1.2321 H
atom	0.0000	-0.9289	1.2321 H
atom	0.0000	0.9289	-1.2321 H
atom	0.0000	-0.9289	-1.2321 H
```

* Here are the commands for the LRTDDFT calculations in the `control.in` file. The kernel is set to be the LDA XC kernel via the `tddft_kernel`,`tddft_x` and `tddft_c` keywords. There is also the basis set information for different species. As recommended in our paper,[^1] we use Tier2 + aug2 as a relatively converged basis sets for molecular LRTDDFT calculation. We can find these basis set information in the folder `AIMS_DIR/species_defaults/non-standard/Tier2_aug2`.
```
  #########################################
    xc                 vwn
    spin none
    relativistic   atomic_zora scalar
    calculate_all_eigenstates

    neutral_excitation   tddft_tda
    tddft_kernel         libxc
    tddft_x              XC_LDA_X
    tddft_c              XC_LDA_C_VWN
    excited_states       20
    excited_mode         both
    empty_states         1000
  #########################################
```

* Run the calculation with the FHI-aims executable.

```
mpirun -n 4 aims.x | tee output
```

* Read the first excitation energy from the output. Given the best theoretical estimation of the first excitation energy to be 7.8 eV.[^5], how good is the result from LRTDDFT? how does it compare to the BSE result?
Here is an example output, where the `6.6558 eV` is the first excitation state.

```
  |    1. Singlet:      6.6558 eV     [  0.2446 Ha]  -  Oscillator Strength:       0.0610
  |            Transition Moments    X:   -0.6118   Y:   -0.0000   Z:    0.0000
  |   Dominant single-particle level transitions in this excitation
  |   eigenvector: [occupied ==> virtual, eigenvector element]
  |   8==>  10    0.7069
```

* For convenience, here we provide the full `control.in` file.

```
  #########################################
    xc                 vwn
    spin none
    relativistic   atomic_zora scalar
    calculate_all_eigenstates

    neutral_excitation   tddft_tda
    tddft_kernel         libxc
    tddft_x              XC_LDA_X
    tddft_c              XC_LDA_C_VWN
    excited_states       20
    excited_mode         both
    empty_states         1000

################################################################################
#
#  FHI-aims code project
#  VB, Fritz-Haber Institut, 2007
#
#  Suggested "safe" defaults for H atom (to be pasted into control.in file)
#
################################################################################
  species        H
#     global species definitions
    nucleus             1
    mass                1.00794
#
    l_hartree           8
#
    cut_pot             4.0  2.0  1.0
    basis_dep_cutoff    0.d0
#
    radial_base         24 7.0
    radial_multiplier   6
    angular_grids       specified
      division   0.1930   50
      division   0.3175  110
      division   0.4293  194
      division   0.5066  302
      division   0.5626  434
      division   0.5922  590
      division   0.6227  974
      division   0.6868 1202
      outer_grid  1202

################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      1  s   1.
#     ion occupancy
    ion_occ      1  s   0.5
################################################################################
#
#  Suggested additional basis functions. For production calculations,
#  uncomment them one after another (the most important basis functions are
#  listed first).
#
#  Basis constructed for dimers: 0.5 A, 0.7 A, 1.0 A, 1.5 A, 2.5 A
#
################################################################################
#  "First tier" - improvements: -1014.90 meV to -62.69 meV
     hydro 2 s 2.1
     hydro 2 p 3.5
#  "Second tier" - improvements: -12.89 meV to -1.83 meV
     hydro 1 s 0.85
     hydro 2 p 3.7
     hydro 2 s 1.2
     hydro 3 d 7
#  "Third tier" - improvements: -0.25 meV to -0.12 meV
#     hydro 4 f 11.2
#     hydro 3 p 4.8
#     hydro 4 d 9
#     hydro 3 s 3.2

###############################################################################
#
# Gaussian augmentation functions.
# Obtained by diff cc-pV5Z aug-cc-pV5Z
#
###############################################################################

    gaussian 0 1 0.0207000
    gaussian 1 1 0.0744000


################################################################################
#
#  FHI-aims code project
#  VB, Fritz-Haber Institut, 2007
#
#  Suggested "safe" defaults for C atom (to be pasted into control.in file)
#
################################################################################
  species        C
#     global species definitions
    nucleus             6
    mass                12.0107
#
    l_hartree           8
#
    cut_pot             4.0  2.0  1.0
    basis_dep_cutoff    0.d0
#
    radial_base         34 7.0
    radial_multiplier   6
    angular_grids specified
      division   0.2187   50
      division   0.4416  110
      division   0.6335  194
      division   0.7727  302
      division   0.8772  434
      division   0.9334  590
      division   0.9924  770
      division   1.0230  974
      division   1.5020 1202
      outer_grid  1202

################################################################################
#
#  Definition of "minimal" basis
#
################################################################################
#     valence basis states
    valence      2  s   2.
    valence      2  p   2.
#     ion occupancy
    ion_occ      2  s   1.
    ion_occ      2  p   1.
################################################################################
#
#  Suggested additional basis functions. For production calculations,
#  uncomment them one after another (the most important basis functions are
#  listed first).
#
#  Constructed for dimers: 1.0 A, 1.25 A, 1.5 A, 2.0 A, 3.0 A
#
################################################################################
#  "First tier" - improvements: -1214.57 meV to -155.61 meV
     hydro 2 p 1.7
     hydro 3 d 6
     hydro 2 s 4.9
#  "Second tier" - improvements: -67.75 meV to -5.23 meV
     hydro 4 f 9.8
     hydro 3 p 5.2
     hydro 3 s 4.3
     hydro 5 g 14.4
     hydro 3 d 6.2
#  "Third tier" - improvements: -2.43 meV to -0.60 meV
#     hydro 2 p 5.6
#     hydro 2 s 1.4
#     hydro 3 d 4.9
#     hydro 4 f 11.2
#  "Fourth tier" - improvements: -0.39 meV to -0.18 meV
#     hydro 2 p 2.1
#     hydro 5 g 16.4
#     hydro 4 d 13.2
#     hydro 3 s 13.6
#     hydro 4 f 17.6
#  Further basis functions - improvements: -0.08 meV and below
#     hydro 3 s 2
#     hydro 3 p 6
#     hydro 4 d 20

###############################################################################
#
# Gaussian augmentation functions.
# Obtained by diff cc-pV5Z aug-cc-pV5Z
#
###############################################################################

    gaussian 0 1 0.0394000
    gaussian 1 1 0.0272000
```

 [^1]:
[Liu, Chi, et al. "All-electron ab initio Bethe-Salpeter equation approach to neutral excitations in molecules with numeric atom-centered orbitals." The Journal of chemical physics 152.4 (2020): 044105.](https://aip.scitation.org/doi/pdf/10.1063/1.5123290)
 [^2]:
[Golze, Dorothea, Marc Dvorak, and Patrick Rinke. "The GW compendium: A practical guide to theoretical photoemission spectroscopy." Frontiers in chemistry 7 (2019): 377.](https://www.frontiersin.org/articles/10.3389/fchem.2019.00377/full)
 [^3]:
[Mark E. Casida. Time-Dependent Density Functional Response Theory for Molecules, volume 1, pages 155–192. 11 1995.](https://www.worldscientific.com/doi/abs/10.1142/9789812830586_0005)
 [^4]:
[Onida, Giovanni, Lucia Reining, and Angel Rubio. "Electronic excitations: density-functional versus many-body Green’s-function approaches." Reviews of modern physics 74.2 (2002): 601.](https://journals.aps.org/rmp/abstract/10.1103/RevModPhys.74.601)
 [^5]:
[Schreiber, Marko, et al. "Benchmarks for electronically excited states: CASPT2, CC2, CCSD, and CC3." The Journal of chemical physics 128.13 (2008): 134110.](https://aip.scitation.org/doi/full/10.1063/1.2889385)
 