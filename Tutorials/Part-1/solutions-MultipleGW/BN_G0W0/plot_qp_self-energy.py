#!/usr/bin/env python

# This script solves the QP equation graphically 
# by plotting the real part of the correlation of self-energy 
# matrix elements vs frequency

import re
import numpy as np
import matplotlib.pyplot as plt


state = 6  # HOMO - see output for details
ha = 27.211399  # in eV

# get $Re \Sigma_i^c$
self_energy = np.loadtxt(f"self_energy_analytic_state_{state}.dat")
self_energy[:, 1] *= ha


# get $\omega + V_i^{xc} - \Sigma_i^x - \epsilon_i^{KS}$
out_lines = open('aims.out').read()

# energy levels come on the 8th line after the header; consist only of numbers, dots, minuses and newlines
# unfortunately the next 3 lines after the table are the minuses and 2 newlines
e_lines = re.search(r"^    GW quasi-particle energy levels(?:.*\n){7}((?:[-\s\d.]*\n)*)", 
                    out_lines, 
                    re.MULTILINE).group(1)
energies = np.array([[float(x) for x in line.split()] for line in e_lines.split('\n')[:-3]])

v_xc, sigma_x, eps_ks = energies[state-1][[4, 3, 2]]

# solve the equation (find 2 roots)
idxs = np.argsort(np.abs( self_energy[:, 1] - (self_energy[:, 0] + v_xc - sigma_x - eps_ks) ))[[0, 2]]

# plot the results
plt.plot(self_energy[:, 0], self_energy[:, 1], label=r'G$_0$W$_0$@PBE')
plt.plot(self_energy[:, 0], self_energy[:, 0] + v_xc - sigma_x - eps_ks, 
         label=r'$\omega + V_i^{xc} - \Sigma_i^x - \epsilon_i^{KS}$')
plt.xlim(-15, -8)
plt.ylim(-10, 10)
plt.xlabel(r'$\omega$ [eV]')
plt.ylabel(r'$Re \Sigma_i^c(\omega)$ [eV]')
# Annotate the results
for idx in idxs:
    x_sol = self_energy[idx, 0]
    plt.axvline(x_sol, -10, 10, ls=':', color='grey')
    plt.annotate(fr'Solution: $\omega$ = {x_sol:.2f} eV',
                color='red',
                xy=(x_sol, self_energy[idx, 1]),
                xytext=(0.15, 0.75) if x_sol < -11.5 else (0.6, 0.25),
                textcoords='figure fraction',
                arrowprops=dict(arrowstyle="->",
                                connectionstyle="arc3,rad=-0.2",
                                color="r"))

plt.legend()
plt.savefig('graphical_solution_g0w0_bn.png')
plt.show()