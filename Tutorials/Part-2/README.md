# GW for solids

Here, we will discuss the band structure of bulk silicon in the diamond structure, calculated with the G$_0$W$_0$ approach based on the DFT-PBE functional. This is perhaps the most important standard example of G$_0$W$_0$ calculations and, as shown below, the actual technical keywords to be used are simple. Further details on this particular implementation and on the meaning of technical choices made can be found in[^1].

The steps to check the convergence of the calculation are important to follow in any GW calculation, as one could obtain a seemingly ``correct'' value e.g. a band gap, but this could be simply due to error cancellation, and in fact you are far from convergence. The situation is not so extreme for bulk silicon as it converges relatively well, but could be the case for your, unknown, material.

!!! Warning "Warning: periodic GW calculations are expensive."

    Periodic G$_0$W$_0$ computations are computationally very demanding, with the current code version, an 8x8x8 k-point grid with intermediate_gw species defaults will take approximately 4 hours on 288 cores. If you do not have access to a compute cluster, for the specific calculations in this tutorial we recommend to survey the input and output files, which are provided here: <https://gitlab.com/FHI-aims-club/tutorials/rpa-and-gw-for-molecules-and-solids/-/tree/main/Tutorials/Part-2/solutions/>


## Requesting a G₀W₀ calculation for periodic solids
  
We will use the `geometry.in` file from the tutorial [Basics of Running FHI-aims, Part 3](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/3-Periodic-Systems/), which has been obtained from a structure optimization based on the HSE06 functional using intermediate species defaults (found in the solutions folder of this tutorial, namely `Si/HSE06_followup_relaxation_intermediate/`)
```
lattice_vector      0.00000003      2.72577696      2.72577687
lattice_vector      2.72577679     -0.00000014      2.72577675
lattice_vector      2.72577695      2.72577699      0.00000011
atom_frac       0.00000000      0.00000000     -0.00000001 Si
atom_frac       0.25000000      0.25000000      0.25000001 Si
```

In FHI-aims, a G$_0$W$_0$ calculation can be requested using the following keywords in the `control.in` file:

```
xc                pbe
relativistic      atomic_zora scalar
qpe_calc          gw_expt
frequency_points  60
anacon_type       0
k_grid            8 8 8
periodic_gw_optimize inverse
output                     band   0.00000  0.00000  0.00000   0.50000  0.00000  0.50000   17 G  X
output                     band   0.50000  0.00000  0.50000   0.50000  0.25000  0.75000   8 X  W
output                     band   0.50000  0.25000  0.75000   0.37500  0.37500  0.75000   6 W  K
output                     band   0.37500  0.37500  0.75000   0.00000  0.00000  0.00000   18 K  G
output                     band   0.00000  0.00000  0.00000   0.50000  0.50000  0.50000   15 G  L
output                     band   0.50000  0.50000  0.50000   0.62500  0.25000  0.62500   10 L  U
output                     band   0.62500  0.25000  0.62500   0.50000  0.25000  0.75000   6 U  W
output                     band   0.50000  0.25000  0.75000   0.50000  0.50000  0.50000   12 W  L
output                     band   0.50000  0.50000  0.50000   0.37500  0.37500  0.75000   10 L  K
output                     band   0.62500  0.25000  0.62500   0.50000  0.00000  0.50000   6 U  X


[attach intermediate_gw species defaults for Si]
```

This input file will trigger the following calculation: 

- First, a normal DFT calculation with the PBE functional will be executed. 

- After the DFT-PBE SCF cycle has converged, the G$_0$W$_0$ calculation will start. This calculation is requested by the keyword `qpe_calc gw_expt`. The argument `gw_expt` indicates that this is a rather new feature (experimental) in FHI-aims and any results should always checked carefully. 

- The keyword `anacon_type` specifies the type of analytical continuation used to extrapolate the self-energy from the imaginary frequency axis to the real-valued frequency axis. The argument `0` means that the two-pole approximation will be used. 

- The keyword `periodic_gw_optimize inverse` enforces FHI-aims to employ a newly implemented performance optimization feature (Cholesky Decomposition for the G$_0$W$_0$ eigenvalue problem), which will save some additional time in G$_0$W$_0$ calculations. This enhancement will likely become the default setting in the near future. We are also anticipating further efficiency improvements elsewhere in the future, so check back with later code versions over time. If there are any issues with the calculation, one should switch this setting off with `periodic_gw_optimize wo_inverse`.

!!! Warning "Warning: Request of band output mandatory!"

    It is necessary to request the band output to obtain the GW eigenvalues. At the moment of writing this tutorial, you will get no GW output, if you don't request `output band`.


In addition, attach the `defaults_2020/intermediate_gw` species default for Si to the above `control.in` file to use intermediate species defaults specific for GW (more on this choice of basis in [this section](#basis) below.)

After the calculation has successfully finished you should find the following files in your folder (see `Tutorials/Part-2/solutions/1-Si-intermediate_gw-band` for solutions):
```
GW_band1001.out
GW_band1002.out
GW_band1003.out
GW_band1004.out
GW_band1005.out
GW_band1006.out
GW_band1007.out
GW_band1008.out
GW_band1009.out
GW_band1010.out
aims.out
control.in
geometry.in
```

The band structure can be simply plotted by uploading the results to [GIMS output analyzer](https://gims.ms1p.org/static/index.html#OutputAnalyzer), or using `clims` (version `>=0.4.2`) using the following command (at the command line in the terminal window), with (optional) energy ranges:
```
clims-aimsplot --emin  -15 --emax 10
```

![](img/bands-Si-GW-int.png)

From the `aims.out` file we can directly read the G$_0$W$_0$ band gap from the first band segment, as the valence band maximum occurs at the Gamma point, and the conduction band minumum to the left of the X point:
```
"GW Band gap" along reciprocal space direction number:    1
| Lowest unoccupied state:          -5.119283 eV
| Highest occupied state :          -6.209232 eV
| Energy difference      :           1.089949 eV

```
This value of 1.09 eV can be compared with the numerically converged literature value of 1.12 eV, as reported in Table 1 in [^1].


In the subsequent sections, we will examine the numerical parameters that need to be tested in order to to obtain a numerically converged GW band structure and are important to follow for any GW calculation, as mentioned in the introduction. The tests to be performed are as follows:

1. Convergence with the basis set used, and why different species defaults are included for GW calculations.
2. Convergence with the number of frequency points used for the self-energy. 
3. Convergence with the number of k-points.

First, however, let us compare the GW result to that from HSE06 and PBE.

## G₀W₀@PBE vs. PBE vs. HSE06 band structure 

The HSE06 and PBE band structures were computed as demonstrated in the [Basics of Running FHI-aims, Part 3](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/3-Periodic-Systems/) tutorial, using FHI-aims intermediate species default settings, 8x8x8 k-space grids, and the same band structure input as the G$_0$W$_0$@PBE case shown above
```
xc              pbe
relativistic    atomic_zora scalar
k_grid          8 8 8

[include band structure input]

[attach intermediate species defaults for Si]
```

```
xc                         hse06 0.11
hse_unit                   bohr
hybrid_xc_coeff            0.25
relativistic               atomic_zora scalar
k_grid                     8 8 8

[include band structure input]

[attach intermediate species defaults for Si]
```
One can compare the GW, HSE06 and PBE band structures by utilising the `aimsplot_compare.py` script inside the `utilities` directory of the FHI-aims install directory, with a command of the form:
```
aimsplot_compare.py N_PLOTS DIRECTORY TITLE ENERGY_OFFSET ... [yMin] [yMax]
```
where `N_PLOTS` is the number of band structures to compare (max 7), `DIRECTORY` is the directory for the aims run of the band structure, `TITLE` is the legend title for the band structure, and `ENERGY_OFFSET` is an energy offset for offseting vertical alignment. The parameters `DIRECTORY`, `TITLE` and `ENERGY_OFFSET` should be entered `N_PLOTS` times. The `yMin` and `yMax` parameters are optional, and set the minimum and maximum energy.

 For example, for the present calculation, one can use a command of the form: 
```
aimsplot_compare.py 3 2-Comparison/PBE-888 "PBE" -0.146 2-Comparison/HSE06-888 "HSE06" -0.213 1-Si-intermediate_gw-band "G0W0@PBE" -0.513 -15 10
```
The offsets were chosen such that the valence band maximum lies at zero. These offsets can be computed from the energy of the highest occupied state at the Gamma point minus the Fermi level. E.g. for GW the highest occupied state is the line in `aims.out`
```
| Highest occupied state :          -6.209232 eV
```
in band segment 1, and the Fermi level is given by the final match of the line
```
| Chemical potential (Fermi level):    -5.69641510 eV
```
i.e. when the SCF cycle is converged. 

!!! Warning "Warning: aimsplot_compare.py only works with Python 2 in this version of the tutorial"

    In order to use Python 3 with `aimsplot_compare.py`, please download it from [here](https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/raw/847183ee246d03c6b008805af211bb5474cfb348/utilities/aimsplot_compare.py?inline=false)

The comparison of the band structure looks like:

![](img/bands-Si-GW-PBE-HSE06.png)


As can be seen in the plot above, there are significant differences between all three methods. Near the band gap, G$_0$W$_0$@PBE and HSE06 agree qualitatively reasonably well for this case (this is, of course, one of the key reasons that hybrid density functionals are often used in lieu of much more expensive but more accurate G$_0$W$_0$ calculations in the literature). DFT-PBE is clearly not sufficient to predict the band gap (we expected nothing else). 

However, for energy bands away from the gap, there are also undeniable differences between G$_0$W$_0$@PBE and HSE06 and, generally, the detailed screening model embodied in G$_0$W$_0$ would be expected to be closer to the actual behavior of these quasiparticle states, e.g., in photoemission spectroscopy.

The complete solutions to this part can be found in `Tutorials/Part-2/solutions/2-Comparison`.


## <a name="basis"></a> Convergence with the basis set

The biggest impact on the numerical convergence of G$_0$W$_0$ calculations arises from the basis set. As seen in other parts of this tutorial, the basis set can make a drastic difference in non-periodic RPA or G$_0$W$_0$ calculations of small molecules. 

For solids and for G$_0$W$_0$ calculations, the situation appears to be somewhat more benign, since basis functions from different unit cells overlap and create an overall higher density of basis functions per volume (better numerical representation). Still, Table I in[^1] shows that for some materials the influence of the basis set is very significant - not just in FHI-aims, but in other G$_0$W$_0$ codes in the community as well. As laid out in[^1] and elsewhere, it is not just the basis set used to expand the orbitals but also the so-called auxiliary basis set used to represent the Coulomb operator that could matter. The `_gw` species defaults in FHI-aims include these auxiliary basis sets.

One can see the additional functions that are included in the `_gw` species defaults by comparing `intermediate` to `intermediate_gw` and `tight` to `tight_gw` basis sets for Si:

- From `intermediate` to `intermediate_gw` : an additional auxiliary `f` function has been added:  
``` 
for_aux hydro 4 f 0.0
```
- From `tight` to `tight_gw` : two additional auxiliary functions have been added, an `f` and a `g` function: 
``` 
for_aux hydro 4 f 0.0
for_aux hydro 5 g 0.0      
```

These small additions makes a difference. Note that these high-angular momentum functions are not included in the basis set used to expand orbitals, but the high-angular momentum function does improve the representation of the Coulomb operator itself. The details of this modification, which is triggered by the `for_aux` keyword, are explained in <http://dx.doi.org/10.1088/1367-2630/17/9/093020> as well as the improvements with respect to the standard species defaults (i.e. those without the additional `for_aux` functions).

Now, let us study the convergence behaviour with respect to the basis set, and the effect of the auxiliary functions for the 8x8x8 k-point grid with 60 frequency points. Create seperate folders for the following species defaults:
```
intermediate
tight_gw
really_tight_gw
```
In each folder, attach the corresponding species defaults to the `control.in` files, with the same settings as the `intermediate_gw` case shown above, which will also be used in the comparison below.

One can see the convergence with respect to the basis set from the computed band gap, with the tight_gw and really-tight_gw approaching the fully converged 1.12 eV value.

![](img/GW-basis-convergence-Si.png)

Verify these values of the band gaps in the output files. `Tutorials/Part-2/solutions/3-basis` contains the solutions for this part.

One key point is that the species defaults matter and for GW calculations it is important to include the auxiliary functions with the `_gw` species defaults. It should be noted that these do introduce additional computational time though, with the `intermediate` calculation taking approximately 2.5 hours, whilst the `intermediate_gw` calculation took 4 hours on 276 cores. The cost of the additional basis functions in the tight_gw and really-tight_gw calculations adds further cost, with these calculations taking approximately 6 hours on 576 cores.


## Convergence with the number of frequency points 

Similar to G$_0$W$_0$ calculations for non-periodic systems, it is necessary to verify that the frequency grid for self-energy calculations is sufficiently well converged. So, let us test with 20, 40, and 80 frequency points as well as our previously computed 60 frequency points by updating the keyword

```
frequency_points  20
frequency_points  40
frequency_points  80
```

Create new folders for each of these calculations, using the `intermediate_gw` species defaults. Copy over the earlier `geometry.in` and `control.in` files and change the number of frequency points in `control.in`. The resulting band gaps look like the following:

![](img/GW-freq-convergence-Si.png)

One can clearly see the convergence with this parameter, with the largest change between 20 and 40 frequency points, a smaller change from 40 to 60, and the smallest change from 60 to 80 frequency points. Ultimately, for Si, these magnitude of these differences is not huge, however for other materials  the number of frequency points can make a more significant difference. The solutions for this part can be found under `Tutorials/Part-2/solutions/4-frq/` directory.



## Convergence with the number of k-points 

We finally study the numerical convergence of G$_0$W$_0$ for bulk Si with the chosen k-space grid. This is another very important test, and we will compare the following settings to our previously computed 8x8x8 k-grid (create a folder for each one and modify the `control.in` file accordingly):
```
k_grid 4 4 4
k_grid 6 6 6
``` 

In order to reflect our findings from the preceding exercises, `intermediate_gw` settings are used. The difference in the computed band gaps is:

![](img/GW-k-grid-convergence-Si.png)

Like with the number of frequency points, one can again observe the convergence behaviour, with a larger difference from the 4x4x4 k-grid to the 6x6x6 k-grid than the 6x6x6 k-grid to the 8x8x8 k-grid.
The computational resources needed for a better k-space grid have, unfortunately, increased quite drastically. From a technical point of view, this is due to a double sum over k-points that leads to N$_k^2$ scaling, where N$_k$ is the number of k-points considered. You can find the corresponding solutions in `Tutorials/Part-2/solutions/5-kgrid` for your reference.

!!! Warning " Particularly fast k-point grid convergence for the Si diamond structure"

    The convergence of the G$_0$W$_0$ band gap for the Si in the diamond structure with the number of k-points is actually quite fast. Other systems might need even denser k-space grids for convergence.


## Conclusion: So, why G₀W₀ instead of computationally cheaper choices?

Importantly, the G$_0$W$_0$ approach cures the significant inaccuracy of the Kohn-Sham band gap of "simple" DFT-PBE when (incorrectly) compared to the experimental quasiparticle band gap of a material.

Furthermore, the G$_0$W$_0$ approach frequently allows one to obtain an essentially parameter-free estimate of the band gap and the band structure with reasonable accuracy already based on a single, simple DFT functional such as PBE. 

In contrast, hybrid density functionals (such a HSE06) are frequently employed with hand-optimized parameters (the exchange mixing parameters) since the "optimal" parameterization of hybrid functionals actually varies with the band gap. In complex materials that contain two or more different components, the so-called "optimal" parameterization of hybrid density functionals often cannot be attained for both components at once. Furthermore, those parameterizations are only formally justified (to an extent) for the band gap itself, not for the remaining energy band structure.

The G$_0$W$_0$ approach cures these downsides of computationally simpler methods such as HSE06. While the method is currently computationally more expensive, it is (in the authors' view) ultimately more physically robust than the available, simpler alternatives - even given the more detailed effort needed to quantify the technical uncertainties (degree of numerical convergence achieved) that still accompany the method.


[^1]:
  Xinguo Ren et al., All-electron periodic G0W0 implementation with numerical atomic orbital basis functions: algorithm and benchmarks. Phys. Rev. Materials 5, 013807 (2021). <https://doi.org/10.1103/PhysRevMaterials.5.013807>

[^2]:
  Arvid Ihrig et al., Accurate localized resolution of identity approach for linear-scaling hybrid density functionals and for many-body perturbation theory. New Journal of Physics 17, 093020 (2015). <http://dx.doi.org/10.1088/1367-2630/17/9/093020>
